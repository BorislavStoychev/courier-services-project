package com.courierservices.presenter.service;

import com.courierservices.model.pojo.Credentials;

/**
 * @author Yordan Marinov
 */
public final class CredentialsService {

    public void save(Credentials credentials){
        TransactionMaker.makeTransaction(session-> session.save(credentials));
    }

    public Credentials get(String username){
        return (Credentials)TransactionMaker.makeGenericTransaction(session->session.get(Credentials.class,username));
    }
}
