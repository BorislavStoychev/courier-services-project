package com.courierservices.presenter.service;

import com.courierservices.model.pojo.Client;
import java.util.List;

/**
 * @author Borislav Stoychev
 */

public final class ClientService {

    public Long save(Client client){
        return (Long) TransactionMaker.makeGenericTransaction(session-> session.save(client));
    }

    @SuppressWarnings("unchecked")
    public List<Client> getAll(){
        return (List<Client>)TransactionMaker.makeGenericTransaction(session ->
                session.createQuery("from Client", Client.class).list());
    }

    public Client get(Long id){
        return (Client)TransactionMaker.makeGenericTransaction(session->session.get(Client.class, id));
    }
}
