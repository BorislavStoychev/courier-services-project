package com.courierservices.presenter.service.api;

import org.hibernate.Session;

/**
 * @author Borislav Stoychev
 */
@FunctionalInterface
public interface GenericTransactionAction
{
    Object action(Session session);
}
