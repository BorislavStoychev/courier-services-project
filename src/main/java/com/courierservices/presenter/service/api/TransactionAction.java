package com.courierservices.presenter.service.api;

import org.hibernate.Session;

/**
 * @author Borislav Stoychev
 */
@FunctionalInterface
public interface TransactionAction
{
    void action(Session session);
}
