package com.courierservices.presenter.service;

import com.courierservices.model.connection.SessionBuilder;
import com.courierservices.presenter.service.api.GenericTransactionAction;
import com.courierservices.presenter.service.api.TransactionAction;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Borislav Stoychev
 */
final class TransactionMaker {
    private static final Logger logger
            = LoggerFactory.getLogger(TransactionMaker.class);

    static int makeTransaction(TransactionAction use) {
        Transaction transaction = null;
        try (Session session = SessionBuilder.getSession()) {
            transaction = session.getTransaction();
            transaction.begin();
            use.action(session);
            transaction.commit();
            return 0;
        } catch (HibernateException hibernateException) {
            if (transaction != null)
                transaction.rollback();
            logger.error(hibernateException.getMessage());
        }
        return -1;
    }

    static Object makeGenericTransaction(GenericTransactionAction use) {
        Transaction transaction = null;
        try (Session session = SessionBuilder.getSession()) {
            transaction = session.getTransaction();
            transaction.begin();
            Object result = use.action(session);
            transaction.commit();
            return result;
        } catch (HibernateException hibernateException) {
            if (transaction != null)
                transaction.rollback();
            logger.error(hibernateException.getMessage());
        }
        return null;
    }
}
