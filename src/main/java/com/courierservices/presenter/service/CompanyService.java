package com.courierservices.presenter.service;

import com.courierservices.model.pojo.Company;

import java.util.List;

/**
 * @author Yordan Marinov
 */
public final class CompanyService {

    public Long save(Company company){
        return (Long) TransactionMaker.makeGenericTransaction(session-> session.save(company));
    }

    @SuppressWarnings("unchecked")
    public List<Company> getAll(){
        return (List<Company>)TransactionMaker.makeGenericTransaction(session->
                session.createQuery("from Company", Company.class).list());
    }

    @SuppressWarnings("unchecked")
    public List<Company> getAllActive(){
        return (List<Company>)TransactionMaker.makeGenericTransaction(session->
                session.createQuery("from Company where inactiveStatus=false", Company.class).list());
    }

    public Company get(Long id){
        return (Company)TransactionMaker.makeGenericTransaction(session->session.get(Company.class,id));
    }

    public int update(Company company){
        return TransactionMaker.makeTransaction(session -> session.update(company));
    }
}
