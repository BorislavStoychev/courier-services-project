package com.courierservices.presenter.service;

import com.courierservices.model.pojo.Courier;
import com.courierservices.model.pojo.Shipment;

import java.util.List;

/**
 * @author Borislav Stoychev
 */

public final class ShipmentService {

    public Long save(Shipment shipment){
        return (Long) TransactionMaker.makeGenericTransaction(session-> session.save(shipment));
    }

    @SuppressWarnings("unchecked")
    public List<Shipment> getAll(){
        return (List<Shipment>)TransactionMaker.makeGenericTransaction(session->
                session.createQuery("from Shipment", Shipment.class).list());
    }

    public Shipment get(Long id){
        return (Shipment) TransactionMaker.makeGenericTransaction(session->session.get(Shipment.class,id));
    }

    @SuppressWarnings("unchecked")
    public List<Shipment> getAll(Courier courier){
        return (List<Shipment>) TransactionMaker.makeGenericTransaction(session->
                session.createQuery("from Shipment where courier=:selectedCourier", Shipment.class)
                .setParameter("selectedCourier", courier).list());
    }
}
