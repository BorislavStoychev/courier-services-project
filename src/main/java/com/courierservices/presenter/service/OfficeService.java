package com.courierservices.presenter.service;

import com.courierservices.model.pojo.Company;
import com.courierservices.model.pojo.Office;

import java.util.List;

/**
 * @author Yordan Marinov
 */
public final class OfficeService {

    public Long save(Office office){
        return (Long) TransactionMaker.makeGenericTransaction(session-> session.save(office));
    }

    @SuppressWarnings("unchecked")
    public List<Office> getAll(){
        return (List<Office>)TransactionMaker.makeGenericTransaction(session->
                session.createQuery("from Office", Office.class).list());
    }

    @SuppressWarnings("unchecked")
    public List<Office> getAllActive(){
        return (List<Office>)TransactionMaker.makeGenericTransaction(session->
                session.createQuery("from Office where inactiveStatus=false", Office.class).list());
    }

    public Office get(Long id){
        return (Office)TransactionMaker.makeGenericTransaction(session->session.get(Office.class,id));
    }

    public int update(Office office){
        return TransactionMaker.makeTransaction(session -> session.update(office));
    }

    @SuppressWarnings("unchecked")
    public List<Office> getAll(Company company){
        return (List<Office>)TransactionMaker.makeGenericTransaction(session->
                session.createQuery(
                        "from Office where company=:ownerCompany and inactiveStatus=false", Office.class)
                        .setParameter("ownerCompany", company)
                        .list());
    }
}
