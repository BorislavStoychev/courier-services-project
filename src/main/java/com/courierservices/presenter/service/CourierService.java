package com.courierservices.presenter.service;

import com.courierservices.model.pojo.Courier;

import java.util.List;

/**
 * @author Borislav Stoychev
 */

public final class CourierService {

    public Long save(Courier courier){
        return (Long) TransactionMaker.makeGenericTransaction(session-> session.save(courier));
    }

    @SuppressWarnings("unchecked")
    public List<Courier> getAll(){
        return (List<Courier>)TransactionMaker.makeGenericTransaction(session->
                session.createQuery("from Courier", Courier.class).list());
    }

    @SuppressWarnings("unchecked")
    public List<Courier> getAllActive(){
        return (List<Courier>)TransactionMaker.makeGenericTransaction(session->
                session.createQuery("from Courier where inactiveStatus=false", Courier.class).list());
    }

    public Courier get(Long id){
        return (Courier)TransactionMaker.makeGenericTransaction(session->session.get(Courier.class,id));
    }

    public int update(Courier courier){
        return TransactionMaker.makeTransaction(session -> session.update(courier));
    }
}
