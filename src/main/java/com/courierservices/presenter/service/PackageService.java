package com.courierservices.presenter.service;

import com.courierservices.model.pojo.Client;
import com.courierservices.model.pojo.Office;
import com.courierservices.model.pojo.Package;
import com.courierservices.model.pojo.PackageStatus;
import com.courierservices.presenter.util.CurrentTimestamp;

import java.util.List;

/**
 * @author Borislav Stoychev
 */

public final class PackageService {

    public Long save(Package pack) {
        return (Long) TransactionMaker.makeGenericTransaction(session -> session.save(pack));
    }

    @SuppressWarnings("unchecked")
    public List<Package> getAll() {
        return (List<Package>) TransactionMaker.makeGenericTransaction(session ->
                session.createQuery("from Package", Package.class).list());
    }

    public Package get(Long id) {
        return (Package) TransactionMaker.makeGenericTransaction(session -> session.get(Package.class, id));
    }

    public int update(Package pack) {
        return TransactionMaker.makeTransaction(session -> session.update(pack));
    }

    @SuppressWarnings("unchecked")
    public List<Package> getAll(Office office) {
        return (List<Package>) TransactionMaker.makeGenericTransaction(session ->
                session.createQuery("from Package where office=:office", Package.class)
                        .setParameter("office", office)
                        .list());
    }

    @SuppressWarnings("unchecked")
    public List<Package> getAll(Office office, String status) {
        return (List<Package>) TransactionMaker.makeGenericTransaction(session ->
                session.createQuery("from Package where office=:office and status=:status", Package.class)
                        .setParameter("office", office)
                        .setParameter("status", status)
                        .list());
    }

    @SuppressWarnings("unchecked")
    public List<Package> getDeliveredPackages(Client client) {
        return (List<Package>) TransactionMaker.makeGenericTransaction(session -> session
                .createQuery("FROM Package " +
                        "WHERE receiver=:receiverParam " +
                        "AND arrivalTime<=:arrivalTimeParam " +
                        "AND status=:statusParam", Package.class)
                .setParameter("receiverParam", client)
                .setParameter("arrivalTimeParam", CurrentTimestamp.get())
                .setParameter("statusParam", PackageStatus.INTRANSIT).list());
    }

    @SuppressWarnings("unchecked")
    public List<Package> getClientPackages(Client client) {
        return (List<Package>) TransactionMaker.makeGenericTransaction(session -> session
                .createQuery("FROM Package " +
                        "WHERE (receiver=:clientParam) " +
                        "OR (sender=:clientParam)", Package.class)
                .setParameter("clientParam", client).list());
    }
}
