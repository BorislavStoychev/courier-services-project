package com.courierservices.presenter.controller;

import com.courierservices.presenter.controller.subcontroller.*;
import com.courierservices.presenter.util.ControllerLoader;
import com.courierservices.view.ViewManager;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author Yordan Marinov
 */
public final class AdministratorController extends ControllerLoader implements Initializable {
    @FXML
    private MenuItem companyClientsStatisticsSubmenu;
    @FXML
    private MenuItem courierWorkStatisticsSubmenu;
    @FXML
    private Button logoutButton;
    @FXML
    private MenuItem companyCreateSubmenu;
    @FXML
    private MenuItem companyUpdateSubmenu;
    @FXML
    private MenuItem courierCreateSubmenu;
    @FXML
    private MenuItem courierUpdateSubmenu;
    @FXML
    private MenuItem officeCreateSubmenu;
    @FXML
    private MenuItem officeUpdateSubmenu;
    @FXML
    private Pane mainPane;

    private Pane activePane = null;

    private void initCompanyCreateSubmenuOnAction() {
        companyCreateSubmenu.setOnAction(event -> {
            String controllerName = "company_create";
            if (!panes.containsKey(controllerName))
                loadController("fxml/company_create.fxml");
            setPane(controllerName);
        });
    }

    private void initCompanyUpdateSubmenuOnAction() {
        companyUpdateSubmenu.setOnAction(event -> {
            String controllerName = "company_update";
            if (!panes.containsKey(controllerName))
                loadController("fxml/company_update.fxml");
            setPane(controllerName);
        });
    }

    private void initOfficeCreateSubmenuOnAction() {
        officeCreateSubmenu.setOnAction(event -> {
            String controllerName = "office_create";
            if (!panes.containsKey(controllerName))
                loadController("fxml/office_create.fxml");
            setPane(controllerName);
        });
    }

    private void initOfficeUpdateSubmenuOnAction() {
        officeUpdateSubmenu.setOnAction(event -> {
            String controllerName = "office_update";
            if (!panes.containsKey(controllerName))
                loadController("fxml/office_update.fxml");
            setPane(controllerName);
        });
    }

    private void initCourierCreateSubmenuOnAction() {
        courierCreateSubmenu.setOnAction(event -> {
            String controllerName = "courier_create";
            if (!panes.containsKey(controllerName))
                loadController("fxml/courier_create.fxml");
            setPane(controllerName);
        });
    }

    private void initCourierUpdateSubmenuOnAction() {
        courierUpdateSubmenu.setOnAction(event -> {
            String controllerName = "courier_update";
            if (!panes.containsKey(controllerName))
                loadController("fxml/courier_update.fxml");
            setPane(controllerName);
        });
    }

    private void initCourierWorkStatisticsOnAction() {
        courierWorkStatisticsSubmenu.setOnAction(event -> {
            String controllerName = "courier_work_statistics";
            if (!panes.containsKey(controllerName))
                loadController("fxml/courier_work_statistics.fxml");
            setPane(controllerName);
        });
    }

    private void initCompanyClientsStatisticsOnAction() {
        companyClientsStatisticsSubmenu.setOnAction(event -> {
            String controllerName = "company_clients_statistics";
            if (!panes.containsKey(controllerName))
                loadController("fxml/company_clients_statistics.fxml");
            setPane(controllerName);
        });
    }

    private void setPane(String pane) {
        mainPane.getChildren().remove(activePane);
        activePane = panes.get(pane);
        mainPane.getChildren().add(activePane);
    }

    private void stopUpdateServices() {
        CompanyUpdateController.stopUpdate();
        OfficeCreateController.stopUpdate();
        CourierCreateController.stopUpdate();
        CourierUpdateController.stopUpdate();
        OfficeUpdateController.stopUpdate();
    }

    private void initLogoutButton() {
        logoutButton.setOnAction(event -> {
            stopUpdateServices();
            mainPane.getChildren().remove(activePane);
            activePane = null;
            ViewManager.resetScene();
        });
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initCompanyCreateSubmenuOnAction();
        initCompanyUpdateSubmenuOnAction();
        initOfficeCreateSubmenuOnAction();
        initOfficeUpdateSubmenuOnAction();
        initCourierCreateSubmenuOnAction();
        initCourierUpdateSubmenuOnAction();
        initCourierWorkStatisticsOnAction();
        initCompanyClientsStatisticsOnAction();
        initLogoutButton();
    }
}
