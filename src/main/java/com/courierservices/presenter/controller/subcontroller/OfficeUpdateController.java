package com.courierservices.presenter.controller.subcontroller;

import com.courierservices.model.pojo.Address;
import com.courierservices.model.pojo.Company;
import com.courierservices.model.pojo.Office;
import com.courierservices.presenter.service.OfficeService;
import com.courierservices.presenter.util.DatabasePoller;
import com.courierservices.presenter.util.FieldValidator;
import com.courierservices.presenter.util.MultipleFieldValidator;
import com.courierservices.presenter.util.StyleConstants;
import com.courierservices.presenter.util.alerts.ErrorAlert;
import com.courierservices.presenter.util.alerts.InfoAlert;
import com.courierservices.presenter.util.alerts.WarningAlert;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Borislav Stoychev
 */

public final class OfficeUpdateController {
    @FXML
    private Button updateButton;
    @FXML
    private ComboBox<Office> officeComboBox;
    @FXML
    private TextField nameField;
    @FXML
    private TextField streetField;
    @FXML
    private TextField cityField;
    @FXML
    private TextField countryField;
    @FXML
    private TextField postalCodeField;
    @FXML
    private TextField phoneNumberField;
    @FXML
    private ComboBox<Company> companyComboBox;
    @FXML
    private CheckBox closedDownCheckBox;

    private static OfficeService officeService = new OfficeService();
    private static ScheduledService<Void> updateService;
    private static ObservableList<Office> offices = FXCollections.observableArrayList(new ArrayList<>());
    private static ObservableList<Company> companies = FXCollections.observableArrayList(new ArrayList<>());
    private MultipleFieldValidator mfv = new MultipleFieldValidator(6);
    private static final Logger logger
            = LoggerFactory.getLogger(OfficeUpdateController.class);

    public void initialize() {
        officeComboBox.setItems(offices);
        companyComboBox.setItems(companies);
        initOfficeNameListener();
        initOfficeCityListener();
        initOfficeCountryListener();
        initOfficeStreetListener();
        initOfficePostalCodeListener();
        initOfficePhoneNumberListener();
        initComboBoxOnSelectOffice();
        //initCompanyComboBoxOnAction();
        initUpdateOfficeButton();
        initUpdateScheduler();
    }

    private void initOfficeNameListener() {
        nameField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                nameField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(0);
            } else {
                nameField.setStyle(StyleConstants.validField);
                mfv.toggleOn(0);
            }
        });
    }

    private void initOfficeStreetListener() {
        streetField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidStreet(newValue)) {
                streetField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(1);
            } else {
                streetField.setStyle(StyleConstants.validField);
                mfv.toggleOn(1);
            }
        });
    }

    private void initOfficeCityListener() {
        cityField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                cityField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(2);
            } else {
                cityField.setStyle(StyleConstants.validField);
                mfv.toggleOn(2);
            }
        });
    }

    private void initOfficeCountryListener() {
        countryField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                countryField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(3);
            } else {
                countryField.setStyle(StyleConstants.validField);
                mfv.toggleOn(3);
            }
        });
    }

    private void initOfficePostalCodeListener() {
        postalCodeField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidPostCode(newValue)) {
                postalCodeField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(4);
            } else {
                postalCodeField.setStyle(StyleConstants.validField);
                mfv.toggleOn(4);
            }
        });
    }

    private void initOfficePhoneNumberListener() {
        phoneNumberField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidPhoneNumber(newValue)) {
                phoneNumberField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(5);
            } else {
                phoneNumberField.setStyle(StyleConstants.validField);
                mfv.toggleOn(5);
            }
        });
    }

    private void loadFields(Office office) {
        if (office != null) {
            Address address = office.getAddress();
            nameField.setText(office.getName());
            closedDownCheckBox.setSelected(office.isInactiveStatus());
            cityField.setText(address.getCity());
            countryField.setText(address.getCountry());
            streetField.setText(address.getStreet());
            postalCodeField.setText(String.valueOf(address.getPostalCode()));
            phoneNumberField.setText(office.getPhoneNumber());
            companyComboBox.getSelectionModel().select(office.getCompany());
        }
    }

    private void initComboBoxOnSelectOffice() {
        officeComboBox.setOnAction(event -> {
            if (updateButton.isDisabled())
                enableComponents();
            loadFields(officeComboBox.getValue());
        });
    }

    private void enableComponents() {
        nameField.setDisable(false);
        streetField.setDisable(false);
        cityField.setDisable(false);
        countryField.setDisable(false);
        postalCodeField.setDisable(false);
        phoneNumberField.setDisable(false);
        companyComboBox.setDisable(false);
        closedDownCheckBox.setDisable(false);
        updateButton.setDisable(false);
    }

    private void disableComponents() {
        nameField.setDisable(true);
        streetField.setDisable(true);
        cityField.setDisable(true);
        countryField.setDisable(true);
        postalCodeField.setDisable(true);
        phoneNumberField.setDisable(true);
        companyComboBox.setDisable(true);
        closedDownCheckBox.setDisable(true);
        updateButton.setDisable(true);
    }

    private void clearComponents() {
        nameField.clear();
        streetField.clear();
        cityField.clear();
        countryField.clear();
        postalCodeField.clear();
        phoneNumberField.clear();
        officeComboBox.getSelectionModel().clearSelection();
        companyComboBox.getSelectionModel().clearSelection();
        mfv.reset();
    }

    public void update(List<Office> updatedOffices, List<Company> updateCompanies) {
        if (officeComboBox.getSelectionModel().isEmpty()) {
            offices.addAll(updatedOffices);
        }
        if (companyComboBox.getSelectionModel().isEmpty()) {
            companies.addAll(updateCompanies);
        }
    }

    private void initUpdateScheduler() {
        updateService = new ScheduledService<Void>() {
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    protected Void call() {
                        List<Office> fetchedOffices = DatabasePoller.fetchOfficeUpdates(offices, false);
                        List<Company> fetchedCompanies = DatabasePoller.fetchCompanyUpdates(companies, true);
                        Platform.runLater(() -> update(fetchedOffices, fetchedCompanies));
                        return null;
                    }
                };
            }
        };
        updateService.setPeriod(Duration.seconds(5));
        updateService.start();
    }

    public static void stopUpdate() {
        if (updateService != null)
            updateService.cancel();
    }

    private void updateAddress(Address address) {
        address.setStreet(streetField.getText());
        address.setCity(cityField.getText());
        address.setCountry(countryField.getText());
        address.setPostalCode(Integer.parseInt(postalCodeField.getText()));
    }

    private void updateOffice(Office office) {
        office.setName(nameField.getText());
        office.setPhoneNumber(phoneNumberField.getText());
        office.setInactiveStatus(closedDownCheckBox.isSelected());
        office.setCompany(companyComboBox.getSelectionModel().getSelectedItem());
        updateAddress(office.getAddress());
    }

    private void initUpdateOfficeButton() {
        updateButton.setOnAction(event ->
        {
            if (mfv.isInvalidObject()) {
                WarningAlert.showAndWait("Please make sure to input correct data in all fields!");
                logger.warn(WarningAlert.getContentText());
            } else {
                Office selectedOffice = officeComboBox.getSelectionModel().getSelectedItem();
                updateOffice(selectedOffice);
                int id = officeService.update(selectedOffice);
                offices.remove(officeComboBox.getSelectionModel().getSelectedItem());
                if (id == -1) {
                    ErrorAlert.showAndWait("An error occurred while trying to update a office!");
                    logger.error(ErrorAlert.getContentText());
                } else {
                    InfoAlert.showAndWait("Office successfully updated!");
                    logger.info(InfoAlert.getContentText());
                    clearComponents();
                    disableComponents();
                }
            }
        });
    }
}