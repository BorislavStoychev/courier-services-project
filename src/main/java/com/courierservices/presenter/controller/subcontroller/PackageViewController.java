package com.courierservices.presenter.controller.subcontroller;

import com.courierservices.model.pojo.Address;
import com.courierservices.model.pojo.Client;
import com.courierservices.model.pojo.Package;
import com.courierservices.presenter.controller.CourierController;
import com.courierservices.presenter.util.DatabasePoller;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Duration;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author Yordan Marinov
 */
public final class PackageViewController implements Initializable {
    @FXML
    private TableView<Package> packagesTable;
    @FXML
    private TableColumn<Package, Client> senderColumn;
    @FXML
    private TableColumn<Package, Client> receiverColumn;
    @FXML
    private TableColumn<Package, String> typeColumn;
    @FXML
    private TableColumn<Package, Double> priceColumn;
    @FXML
    private TableColumn<Package, Address> destinationColumn;
    @FXML
    private TableColumn<Package, String> statusColumn;

    private static ObservableList<Package> packages = FXCollections.observableArrayList(new ArrayList<>());
    private static ScheduledService<Void> updateService;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        packagesTable.setItems(packages);
        initCellProperties();
        initUpdateScheduler();
    }

    private void initCellProperties() {
        senderColumn.setCellValueFactory(new PropertyValueFactory<>("sender"));
        receiverColumn.setCellValueFactory(new PropertyValueFactory<>("receiver"));
        typeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        priceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        destinationColumn.setCellValueFactory(new PropertyValueFactory<>("receiverAddress"));
        statusColumn.setCellValueFactory(new PropertyValueFactory<>("status"));
    }

    private void initUpdateScheduler() {
        updateService = new ScheduledService<Void>() {
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    protected Void call() {
                        List<Package> fetchedPackages = DatabasePoller.fetchPackageUpdates(packages, CourierController.loggedUser.getOffice());
                        Platform.runLater(() -> update(fetchedPackages));
                        return null;
                    }
                };
            }
        };
        updateService.setPeriod(Duration.seconds(5));
        updateService.start();
    }

    public void update(List<Package> updatedPackages) {
        packages.addAll(updatedPackages);
    }

    public static void stopUpdate() {
        if (updateService != null)
            updateService.cancel();
    }
}
