package com.courierservices.presenter.controller.subcontroller;

import com.courierservices.model.pojo.Courier;
import com.courierservices.model.pojo.Office;
import com.courierservices.presenter.service.CourierService;
import com.courierservices.presenter.util.DatabasePoller;
import com.courierservices.presenter.util.FieldValidator;
import com.courierservices.presenter.util.MultipleFieldValidator;
import com.courierservices.presenter.util.StyleConstants;
import com.courierservices.presenter.util.alerts.ErrorAlert;
import com.courierservices.presenter.util.alerts.InfoAlert;
import com.courierservices.presenter.util.alerts.WarningAlert;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Borislav Stoychev
 */

public final class CourierUpdateController {
    @FXML
    private ComboBox<Courier> courierComboBox;
    @FXML
    private TextField firstNameField;
    @FXML
    private TextField middleNameField;
    @FXML
    private TextField lastNameField;
    @FXML
    private TextField emailAddressField;
    @FXML
    private TextField phoneNumberField;
    @FXML
    private ComboBox<Office> officeComboBox;
    @FXML
    private Button updateButton;
    @FXML
    private CheckBox closedDownCheckBox;

    private static CourierService courierService = new CourierService();
    private static ScheduledService<Void> updateService;
    private static ObservableList<Courier> couriers = FXCollections.observableArrayList(new ArrayList<>());
    private static ObservableList<Office> offices = FXCollections.observableArrayList(new ArrayList<>());
    private MultipleFieldValidator mfv = new MultipleFieldValidator(5);
    private static final Logger logger
            = LoggerFactory.getLogger(CourierUpdateController.class);

    public void initialize() {
        courierComboBox.setItems(couriers);
        officeComboBox.setItems(offices);
        initCourierFirstNameListener();
        initCourierMiddleNameListener();
        initCourierLastNameListener();
        initCourierEmailAddressListener();
        initCourierPhoneNumberListener();
        initComboBoxOnSelectCourier();
        initUpdateCourierButton();
        initUpdateScheduler();
    }

    private void initCourierFirstNameListener() {
        firstNameField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                firstNameField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(0);
            } else {
                firstNameField.setStyle(StyleConstants.validField);
                mfv.toggleOn(0);
            }
        });
    }

    private void initCourierMiddleNameListener() {
        middleNameField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                middleNameField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(1);
            } else {
                middleNameField.setStyle(StyleConstants.validField);
                mfv.toggleOn(1);
            }
        });
    }

    private void initCourierLastNameListener() {
        lastNameField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                lastNameField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(2);
            } else {
                lastNameField.setStyle(StyleConstants.validField);
                mfv.toggleOn(2);
            }
        });
    }

    private void initCourierEmailAddressListener() {
        emailAddressField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidEmailAddress(newValue)) {
                emailAddressField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(3);
            } else {
                emailAddressField.setStyle(StyleConstants.validField);
                mfv.toggleOn(3);
            }
        });
    }

    private void initCourierPhoneNumberListener() {
        phoneNumberField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidPhoneNumber(newValue)) {
                phoneNumberField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(4);
            } else {
                phoneNumberField.setStyle(StyleConstants.validField);
                mfv.toggleOn(4);
            }
        });
    }

    private void loadFields(Courier courier) {
        if (courier != null) {
            firstNameField.setText(courier.getFirstName());
            middleNameField.setText(courier.getMiddleName());
            lastNameField.setText(courier.getLastName());
            emailAddressField.setText(courier.getEmailAddress());
            phoneNumberField.setText(courier.getPhoneNumber());
            closedDownCheckBox.setSelected(courier.isInactive());
            officeComboBox.getSelectionModel().select(courier.getOffice());
        }
    }

    private void initComboBoxOnSelectCourier() {
        courierComboBox.setOnAction(event -> {
            if (updateButton.isDisabled())
                enableComponents();
            loadFields(courierComboBox.getValue());
        });
    }

    private void initUpdateScheduler() {
        updateService = new ScheduledService<Void>() {
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    protected Void call() {
                        List<Courier> fetchedCouriers = DatabasePoller.fetchCourierUpdates(couriers);
                        List<Office> fetchedOffices = DatabasePoller.fetchOfficeUpdates(offices, true);
                        Platform.runLater(() -> update(fetchedCouriers, fetchedOffices));
                        return null;
                    }
                };
            }
        };
        updateService.setPeriod(Duration.seconds(5));
        updateService.start();
    }

    public static void stopUpdate() {
        if (updateService != null)
            updateService.cancel();
    }

    private void enableComponents() {
        firstNameField.setDisable(false);
        middleNameField.setDisable(false);
        lastNameField.setDisable(false);
        emailAddressField.setDisable(false);
        phoneNumberField.setDisable(false);
        officeComboBox.setDisable(false);
        closedDownCheckBox.setDisable(false);
        updateButton.setDisable(false);
    }

    private void disableComponents() {
        firstNameField.setDisable(true);
        middleNameField.setDisable(true);
        lastNameField.setDisable(true);
        emailAddressField.setDisable(true);
        phoneNumberField.setDisable(true);
        officeComboBox.setDisable(true);
        closedDownCheckBox.setDisable(true);
        updateButton.setDisable(true);
    }

    public void update(List<Courier> updatedCouriers, List<Office> updatedOffices) {
        if (courierComboBox.getSelectionModel().isEmpty()) {
            couriers.addAll(updatedCouriers);
        }
        if (officeComboBox.getSelectionModel().isEmpty()) {
            offices.addAll(updatedOffices);
        }
    }

    private void clearComponents() {
        firstNameField.clear();
        middleNameField.clear();
        lastNameField.clear();
        emailAddressField.clear();
        phoneNumberField.clear();
        courierComboBox.getSelectionModel().clearSelection();
        officeComboBox.getSelectionModel().clearSelection();
        closedDownCheckBox.setSelected(false);
        mfv.reset();
    }

    private void updateCourier(Courier selectedCourier) {
        selectedCourier.setFirstName(firstNameField.getText());
        selectedCourier.setMiddleName(middleNameField.getText());
        selectedCourier.setLastName(lastNameField.getText());
        selectedCourier.setPhoneNumber(phoneNumberField.getText());
        selectedCourier.setEmailAddress(emailAddressField.getText());
        selectedCourier.setOffice(officeComboBox.getSelectionModel().getSelectedItem());
        selectedCourier.setInactiveStatus(closedDownCheckBox.isSelected());
    }

    private void initUpdateCourierButton() {
        updateButton.setOnAction(event -> {
            if (mfv.isInvalidObject()) {
                WarningAlert.showAndWait("Please make sure to input correct data in all fields!");
                logger.warn(WarningAlert.getContentText());
            } else {
                Courier selectedCourier = courierComboBox.getSelectionModel().getSelectedItem();
                updateCourier(selectedCourier);
                int id = courierService.update(selectedCourier);
                couriers.remove(courierComboBox.getSelectionModel().getSelectedItem());
                if (id == -1) {
                    ErrorAlert.showAndWait("An error occurred while trying to update a courier!");
                    logger.error(ErrorAlert.getContentText());
                } else {
                    InfoAlert.showAndWait("Courier successfully updated!");
                    logger.info(InfoAlert.getContentText());
                    clearComponents();
                    disableComponents();
                }
            }
        });
    }
}
