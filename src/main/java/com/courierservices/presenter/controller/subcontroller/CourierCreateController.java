package com.courierservices.presenter.controller.subcontroller;

import com.courierservices.model.pojo.Courier;
import com.courierservices.model.pojo.Credentials;
import com.courierservices.model.pojo.Office;
import com.courierservices.presenter.service.CourierService;
import com.courierservices.presenter.service.CredentialsService;
import com.courierservices.presenter.util.*;
import com.courierservices.presenter.util.alerts.ErrorAlert;
import com.courierservices.presenter.util.alerts.InfoAlert;
import com.courierservices.presenter.util.alerts.WarningAlert;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Borislav Stoychev
 */

public final class CourierCreateController {
    @FXML
    private Button createButton;
    @FXML
    private Button generateCredentialsButton;
    @FXML
    private TextField usernameField;
    @FXML
    private TextField passwordField;
    @FXML
    private TextField firstNameField;
    @FXML
    private TextField middleNameField;
    @FXML
    private TextField lastNameField;
    @FXML
    private TextField emailAddressField;
    @FXML
    private TextField phoneNumberField;
    @FXML
    private ComboBox<Office> officeComboBox;

    private CourierService courierService = new CourierService();
    private CredentialsService credentialsService = new CredentialsService();
    private static ScheduledService<Void> updateService;
    private static ObservableList<Office> offices = FXCollections.observableArrayList(new ArrayList<>());
    private MultipleFieldValidator mfv = new MultipleFieldValidator(6);
    private static final Logger logger
            = LoggerFactory.getLogger(CourierCreateController.class);

    public void initialize() {
        officeComboBox.setItems(offices);
        initCourierFirstNameListener();
        initCourierMiddleNameListener();
        initCourierLastNameListener();
        initCourierPhoneNumberListener();
        initCourierEmailAddressListener();
        initComboBoxOnSelect();
        initGenerateCredentialsButton();
        initCourierCreateButton();
        initUpdateScheduler();
    }

    private void initGenerateCredentialsButton() {
        generateCredentialsButton.setOnAction(event -> {
            usernameField.setText(UsernameGenerator.generateCourierUsername());
            passwordField.setText(PasswordGenerator.generatePassword());
            createButton.setDisable(false);
        });
    }

    private void initCourierFirstNameListener() {
        firstNameField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                firstNameField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(0);
            } else {
                firstNameField.setStyle(StyleConstants.validField);
                mfv.toggleOn(0);
            }
        });
    }

    private void initCourierMiddleNameListener() {
        middleNameField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                middleNameField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(1);
            } else {
                middleNameField.setStyle(StyleConstants.validField);
                mfv.toggleOn(1);
            }
        });
    }

    private void initCourierLastNameListener() {
        lastNameField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                lastNameField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(2);
            } else {
                lastNameField.setStyle(StyleConstants.validField);
                mfv.toggleOn(2);
            }
        });
    }

    private void initCourierPhoneNumberListener() {
        phoneNumberField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidPhoneNumber(newValue)) {
                phoneNumberField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(3);
            } else {
                phoneNumberField.setStyle(StyleConstants.validField);
                mfv.toggleOn(3);
            }
        });
    }

    private void initCourierEmailAddressListener() {
        emailAddressField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidEmailAddress(newValue)) {
                emailAddressField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(4);
            } else {
                emailAddressField.setStyle(StyleConstants.validField);
                mfv.toggleOn(4);
            }
        });
    }

    private void initComboBoxOnSelect() {
        officeComboBox.setOnAction(event -> {
            if (officeComboBox.getSelectionModel().isEmpty())
                mfv.toggleOff(5);
            else
                mfv.toggleOn(5);
        });
    }

    private void clearComponents() {
        usernameField.clear();
        passwordField.clear();
        firstNameField.clear();
        middleNameField.clear();
        lastNameField.clear();
        phoneNumberField.clear();
        emailAddressField.clear();
        officeComboBox.getSelectionModel().clearSelection();
        mfv.reset();
    }

    private void update(List<Office> updatedOffices) {
        if (officeComboBox.getSelectionModel().isEmpty()) {
            offices.addAll(updatedOffices);
        }
    }

    private void initUpdateScheduler() {
        updateService = new ScheduledService<Void>() {
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    protected Void call() {
                        List<Office> fetchedUpdate = DatabasePoller.fetchOfficeUpdates(offices, true);
                        Platform.runLater(() -> update(fetchedUpdate));
                        return null;
                    }
                };
            }
        };
        updateService.setPeriod(Duration.seconds(5));
        updateService.start();
    }

    public static void stopUpdate() {
        if (updateService != null)
            updateService.cancel();
    }

    private Courier buildCourier() {
        Courier courier = new Courier();
        courier.setFirstName(firstNameField.getText());
        courier.setMiddleName(middleNameField.getText());
        courier.setLastName(lastNameField.getText());
        courier.setPhoneNumber(phoneNumberField.getText());
        courier.setEmailAddress(emailAddressField.getText());
        courier.setOffice(officeComboBox.getValue());
        courier.setCreationDate(CurrentTimestamp.get());
        return courier;
    }

    private Credentials buildCredentials(long id) {
        Credentials credentials = new Credentials();
        credentials.setUsername(usernameField.getText());
        credentials.setPassword(passwordField.getText());
        credentials.setUserId(id);
        return credentials;
    }

    private void initCourierCreateButton() {
        createButton.setOnAction(event -> {
            if (mfv.isInvalidObject()) {
                WarningAlert.showAndWait("Please make sure to input correct data in all fields!");
                logger.warn(WarningAlert.getContentText());
            } else {
                Courier courier = buildCourier();
                long courierId = courierService.save(courier);
                if (courierId == -1) {
                    ErrorAlert.showAndWait("An error occurred while trying to create a courier!");
                    logger.error(ErrorAlert.getContentText());
                } else {
                    Credentials courierCredentials = buildCredentials(courierId);
                    credentialsService.save(courierCredentials);
                    InfoAlert.showAndWait("Courier successfully created!");
                    logger.info(InfoAlert.getContentText());
                    clearComponents();
                }
            }
        });
    }
}