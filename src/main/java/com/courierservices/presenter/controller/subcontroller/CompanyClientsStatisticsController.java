package com.courierservices.presenter.controller.subcontroller;

import com.courierservices.model.pojo.Client;
import com.courierservices.model.pojo.Company;
import com.courierservices.presenter.service.CompanyService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;

import java.net.URL;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Borislav Stoychev
 */

public final class CompanyClientsStatisticsController implements Initializable {
    @FXML
    private Button showButton;
    @FXML
    private PieChart companyClientsPieChart;
    @FXML
    private DatePicker fromDate;
    @FXML
    private DatePicker toDate;

    private final ObservableList<PieChart.Data> data = FXCollections.observableArrayList(new ArrayList<>());
    private final CompanyService companyService = new CompanyService();
    private final Map<PieChart.Data, String> map = new HashMap<>();
    private Timestamp from;
    private Timestamp to;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        companyClientsPieChart.setData(data);
        initShowButton();
    }

    private void initShowButton() {
        showButton.setOnAction(event -> {
            companyClientsPieChart.getData().clear();
            data.clear();
            map.clear();
            if (fromDate.getValue() != null && toDate.getValue() != null) {
                from = Timestamp.valueOf(fromDate.getValue().atStartOfDay());
                to = Timestamp.valueOf(toDate.getValue().atStartOfDay());
                List<Company> companies = companyService.getAllActive();
                constructDiagram(companies);
                fromDate.setValue(null);
                toDate.setValue(null);
            }
        });
    }

    private boolean filterClients(Client client) {
        return client.getCreationDate().after(from) && client.getCreationDate().before(to);
    }

    private PieChart.Data buildSlice(Company company) {
        String companyName = company.getName();
        int clientsNumber = (int) company.getClients().stream().filter(this::filterClients).count();
        return new PieChart.Data(companyName, clientsNumber);
    }

    private void constructDiagram(List<Company> companies) {
        if (companies != null) {
            List<Company> existingCompanies = companies.stream().filter(company ->
                    company.getCreationDate().before(to)).collect(Collectors.toList());
            existingCompanies.forEach(company -> {
                PieChart.Data slice = buildSlice(company);
                if (slice.getPieValue() > 0)
                    data.add(slice);
                map.put(slice, slice.getName());
            });
            initValuesOnHover();
        }
    }

    private void initValuesOnHover() {
        data.forEach(slice -> slice.getNode().setOnMouseEntered(event -> {
            slice.setName(String.valueOf((int) (slice.getPieValue())));
        }));
        data.forEach(slice -> slice.getNode().setOnMouseExited(event -> {
            slice.setName(map.get(slice));
        }));
    }
}