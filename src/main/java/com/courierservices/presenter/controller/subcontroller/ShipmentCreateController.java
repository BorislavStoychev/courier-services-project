package com.courierservices.presenter.controller.subcontroller;

import com.courierservices.model.pojo.Package;
import com.courierservices.model.pojo.*;
import com.courierservices.presenter.controller.CourierController;
import com.courierservices.presenter.service.ShipmentService;
import com.courierservices.presenter.util.CurrentTimestamp;
import com.courierservices.presenter.util.DatabasePoller;
import com.courierservices.presenter.util.NumberGenerator;
import com.courierservices.presenter.util.alerts.ErrorAlert;
import com.courierservices.presenter.util.alerts.InfoAlert;
import com.courierservices.presenter.util.alerts.WarningAlert;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.util.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * @author Yordan Marinov
 */
public final class ShipmentCreateController implements Initializable {
    @FXML
    private ComboBox<String> selectionComboBox;
    @FXML
    private Button sendShipmentButton;
    @FXML
    private ListView<Package> packageListView;
    @FXML
    private ListView<Package> shipmentListView;
    @FXML
    private Button addButton;
    @FXML
    private Button removeButton;

    private ObservableList<Package> activePackages;
    private ObservableList<Package> activeShipment;

    private ObservableList<Package> customPackages = FXCollections.observableArrayList(new ArrayList<>());
    private ObservableList<Package> officePackages = FXCollections.observableArrayList(new ArrayList<>());
    private ObservableList<Package> customShipment = FXCollections.observableArrayList(new ArrayList<>());
    private ObservableList<Package> officeShipment = FXCollections.observableArrayList(new ArrayList<>());
    private ObservableList<Package> packages = FXCollections.observableArrayList(new ArrayList<>());
    private ObservableList<Office> offices = FXCollections.observableArrayList(new ArrayList<>());

    private ShipmentService shipmentService = new ShipmentService();
    private static ScheduledService<Void> updateService;
    private static final Logger logger
            = LoggerFactory.getLogger(ShipmentCreateController.class);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        selectionComboBox.getItems().addAll("Custom destination packages", "Office destination packages");
        initSelectionComboBox();
        initSendShipmentButton();
        initAddButton();
        initRemoveButton();
        initUpdateScheduler();
    }

    private void initSelectionComboBox() {
        selectionComboBox.setOnAction(event -> {
            activePackages = (selectionComboBox.getValue().equals("Custom destination packages"))
                    ? customPackages : officePackages;
            activeShipment = (selectionComboBox.getValue().equals("Custom destination packages"))
                    ? customShipment : officeShipment;
            packageListView.setItems(activePackages);
            shipmentListView.setItems(activeShipment);
        });
    }

    private Shipment buildShipment() {
        Shipment shipment = new Shipment();
        shipment.setDispatchTime(CurrentTimestamp.get());
        shipment.setPackages(activeShipment);
        shipment.setCourier(CourierController.loggedUser);
        updateStatusOfPackages(shipment);
        return shipment;
    }

    private void initSendShipmentButton() {
        sendShipmentButton.setOnAction(event -> {
            if (activeShipment.isEmpty()) {
                WarningAlert.showAndWait("Please make sure to input correct data in all fields!");
                logger.warn(WarningAlert.getContentText());
            } else {
                Shipment shipment = buildShipment();
                long id = shipmentService.save(shipment);
                if (id == -1) {
                    ErrorAlert.showAndWait("An error occurred while trying to create a shipment!");
                    logger.error(ErrorAlert.getContentText());
                } else {
                    packages.removeAll(activeShipment); // Clear all shipments that were sent so they don't appear again!
                    activeShipment.clear();
                    InfoAlert.showAndWait("Shipment successfully created!");
                    logger.info(InfoAlert.getContentText());
                }
            }
        });
    }

    private Timestamp calculateArrivalTime() {
        int delayInDays = ((activeShipment == officeShipment)
                ? NumberGenerator.generateSmallNumber()
                : NumberGenerator.generateBigNumber());
        return new Timestamp(CurrentTimestamp.get().getTime() + 86400000 * delayInDays);
    }

    private void updateStatusOfPackages(Shipment shipment) {
        activeShipment.forEach(pack -> {
            pack.setShipment(shipment);
            pack.setArrivalTime(calculateArrivalTime());
            pack.setStatus(PackageStatus.INTRANSIT);
        });
    }

    private void initAddButton() {
        addButton.setOnAction(event -> {
            Package selectedPackage = packageListView.getSelectionModel().getSelectedItem();
            if (selectedPackage != null) {
                activePackages.remove(selectedPackage);
                activeShipment.add(selectedPackage);
            }
        });
    }

    private void initRemoveButton() {
        removeButton.setOnAction(event -> {
            Package selectedPackage = shipmentListView.getSelectionModel().getSelectedItem();
            if (selectedPackage != null) {
                activePackages.add(selectedPackage);
                activeShipment.remove(selectedPackage);
            }
        });
    }

    private void initUpdateScheduler() {
        updateService = new ScheduledService<Void>() {
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    protected Void call() {
                        List<Office> fetchedOffices =
                                DatabasePoller.fetchOfficeUpdates(offices, CourierController.loggedUser.getOffice().getCompany());
                        List<Package> fetchedPackages =
                                DatabasePoller.fetchPackageUpdates(packages, CourierController.loggedUser.getOffice(), PackageStatus.REGISTERED);
                        Platform.runLater(() -> update(fetchedOffices, fetchedPackages));
                        return null;
                    }
                };
            }
        };
        updateService.setPeriod(Duration.seconds(5));
        updateService.start();
    }

    private List<Address> getOfficeAddresses(List<Office> updatedOffices) {
        return updatedOffices
                .stream()
                .map(Office::getAddress)
                .collect(Collectors.toList());
    }

    public void update(List<Office> updatedOffices, List<Package> updatedPackages) {
        offices.addAll(updatedOffices);
        packages.addAll(updatedPackages);
        List<Address> officeAddresses = getOfficeAddresses(offices);
        if (activePackages != officePackages && officeShipment.isEmpty()) {
            updateOfficePackages(officeAddresses);
        }
        if (activePackages != customPackages && customShipment.isEmpty()) {
            updateCustomPackages(officeAddresses);
        }
    }

    private void updateOfficePackages(List<Address> officeAddresses) {
        officePackages.addAll(packages.stream()
                .filter(pack -> (officeAddresses.contains(pack.getReceiverAddress()) && !officePackages.contains(pack)))
                .collect(Collectors.toList()));
    }

    private void updateCustomPackages(List<Address> officeAddresses) {
        customPackages.addAll(packages.stream()
                .filter(pack -> (!officeAddresses.contains(pack.getReceiverAddress()) && !customPackages.contains(pack)))
                .collect(Collectors.toList()));
    }

    public static void stopUpdate() {
        if (updateService != null)
            updateService.cancel();
    }
}
