package com.courierservices.presenter.controller.subcontroller;

import com.courierservices.model.pojo.Address;
import com.courierservices.model.pojo.Company;
import com.courierservices.model.pojo.Office;
import com.courierservices.presenter.service.OfficeService;
import com.courierservices.presenter.util.*;
import com.courierservices.presenter.util.alerts.ErrorAlert;
import com.courierservices.presenter.util.alerts.InfoAlert;
import com.courierservices.presenter.util.alerts.WarningAlert;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Borislav Stoychev
 */

public final class OfficeCreateController {
    @FXML
    private Button createButton;
    @FXML
    private TextField officeNameField;
    @FXML
    private TextField officeStreetField;
    @FXML
    private TextField officeCityField;
    @FXML
    private TextField officeCountryField;
    @FXML
    private TextField officePostalCodeField;
    @FXML
    private TextField officePhoneNumberField;
    @FXML
    private ComboBox<Company> companyComboBox;

    private OfficeService officeService = new OfficeService();
    private static ObservableList<Company> companies = FXCollections.observableArrayList(new ArrayList<>());
    private MultipleFieldValidator mfv = new MultipleFieldValidator(7);
    private static ScheduledService<Void> updateService;
    private static final Logger logger
            = LoggerFactory.getLogger(OfficeCreateController.class);

    public void initialize() {
        companyComboBox.setItems(companies);
        initOfficeNameListener();
        initOfficeStreetListener();
        initOfficeCityListener();
        initOfficeCountryListener();
        initOfficePostalCodeListener();
        initOfficePhoneNumberListener();
        initCreateOfficeButton();
        initComboBoxOnSelect();
        initUpdateScheduler();
    }

    private void initOfficeNameListener() {
        officeNameField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                officeNameField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(0);
            } else {
                officeNameField.setStyle(StyleConstants.validField);
                mfv.toggleOn(0);
            }
        });
    }

    private void initOfficeStreetListener() {
        officeStreetField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidStreet(newValue)) {
                officeStreetField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(1);
            } else {
                officeStreetField.setStyle(StyleConstants.validField);
                mfv.toggleOn(1);
            }
        });
    }

    private void initOfficeCityListener() {
        officeCityField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                officeCityField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(2);
            } else {
                officeCityField.setStyle(StyleConstants.validField);
                mfv.toggleOn(2);
            }
        });
    }

    private void initOfficeCountryListener() {
        officeCountryField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                officeCountryField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(3);
            } else {
                officeCountryField.setStyle(StyleConstants.validField);
                mfv.toggleOn(3);
            }
        });
    }

    private void initOfficePostalCodeListener() {
        officePostalCodeField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidPostCode(newValue)) {
                officePostalCodeField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(4);
            } else {
                officePostalCodeField.setStyle(StyleConstants.validField);
                mfv.toggleOn(4);
            }
        });
    }

    private void initOfficePhoneNumberListener() {
        officePhoneNumberField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidPhoneNumber(newValue)) {
                officePhoneNumberField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(5);
            } else {
                officePhoneNumberField.setStyle(StyleConstants.validField);
                mfv.toggleOn(5);
            }
        });
    }

    private void initComboBoxOnSelect() {
        companyComboBox.setOnAction(event -> {
            if (companyComboBox.getSelectionModel().isEmpty())
                mfv.toggleOff(6);
            else
                mfv.toggleOn(6);
        });
    }

    private void initUpdateScheduler() {
        updateService = new ScheduledService<Void>() {
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    protected Void call() {
                        List<Company> fetchedCompanies = DatabasePoller.fetchCompanyUpdates(companies, true);
                        Platform.runLater(() -> update(fetchedCompanies));
                        return null;
                    }
                };
            }
        };
        updateService.setPeriod(Duration.seconds(5));
        updateService.start();
    }

    private void clearComponents() {
        officeNameField.clear();
        officeStreetField.clear();
        officeCityField.clear();
        officeCountryField.clear();
        officePostalCodeField.clear();
        officePhoneNumberField.clear();
        companyComboBox.getSelectionModel().clearSelection();
        mfv.reset();
    }

    public void update(List<Company> updatedCompanies) {
        if (companyComboBox.getSelectionModel().isEmpty()) {
            companies.addAll(updatedCompanies);
        }
    }

    public static void stopUpdate() {
        if (updateService != null)
            updateService.cancel();
    }

    private Address buildAddress() {
        Address address = new Address();
        address.setStreet(officeStreetField.getText());
        address.setCity(officeCityField.getText());
        address.setCountry(officeCityField.getText());
        address.setPostalCode(Integer.parseInt(officePostalCodeField.getText()));
        return address;
    }

    private Office buildOffice() {
        Office office = new Office();
        Address address = buildAddress();
        office.setName(officeNameField.getText());
        office.setPhoneNumber(officePhoneNumberField.getText());
        office.setCompany(companyComboBox.getSelectionModel().getSelectedItem());
        office.setAddress(address);
        office.setCreationDate(CurrentTimestamp.get());
        return office;
    }

    private void initCreateOfficeButton() {
        createButton.setOnAction(event -> {
            if (mfv.isInvalidObject()) {
                WarningAlert.showAndWait("Please make sure to input correct data in all fields!");
                logger.warn(WarningAlert.getContentText());
            } else {
                Office office = buildOffice();
                long id = officeService.save(office);
                if (id == -1) {
                    ErrorAlert.showAndWait("An error occurred while trying to create a office!");
                    logger.error(ErrorAlert.getContentText());
                } else {
                    InfoAlert.showAndWait("Office successfully created!");
                    logger.info(InfoAlert.getContentText());
                    clearComponents();
                }
            }
        });
    }
}
