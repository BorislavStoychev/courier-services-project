package com.courierservices.presenter.controller.subcontroller;

import com.courierservices.model.pojo.Package;
import com.courierservices.model.pojo.*;
import com.courierservices.presenter.controller.CourierController;
import com.courierservices.presenter.service.PackageService;
import com.courierservices.presenter.util.CurrentTimestamp;
import com.courierservices.presenter.util.DatabasePoller;
import com.courierservices.presenter.util.MultipleFieldValidator;
import com.courierservices.presenter.util.PriceCalculator;
import com.courierservices.presenter.util.alerts.ErrorAlert;
import com.courierservices.presenter.util.alerts.InfoAlert;
import com.courierservices.presenter.util.alerts.WarningAlert;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.util.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author Yordan Marinov
 */
public final class PackageCreateController implements Initializable {
    @FXML
    private Button registerPackageButton;
    @FXML
    private ComboBox<Client> senderComboBox;
    @FXML
    private ComboBox<Client> receiverComboBox;
    @FXML
    private ComboBox<String> packageTypeComboBox;
    @FXML
    private CheckBox officeDestinationCheckBox;
    @FXML
    private ComboBox<Office> officeComboBox;

    private static PackageService packageService = new PackageService();
    private static ObservableList<Client> clients = FXCollections.observableArrayList(new ArrayList<>());
    private static ObservableList<Office> offices = FXCollections.observableArrayList(new ArrayList<>());
    private static ScheduledService<Void> updateService;
    private MultipleFieldValidator mfv = new MultipleFieldValidator(4);
    private static final Logger logger
            = LoggerFactory.getLogger(PackageCreateController.class);

    public static void stopUpdate() {
        if (updateService != null)
            updateService.cancel();
    }

    private void initSenderComboBoxOnSelect() {
        senderComboBox.setOnAction(event -> {
            if (senderComboBox.getSelectionModel().isEmpty())
                mfv.toggleOff(0);
            else
                mfv.toggleOn(0);
        });
    }

    private void initReceiverComboBoxOnSelect() {
        receiverComboBox.setOnAction(event -> {
            if (receiverComboBox.getSelectionModel().isEmpty())
                mfv.toggleOff(1);
            else
                mfv.toggleOn(1);
        });
    }

    private void initTypeComboBoxOnSelect() {
        packageTypeComboBox.setOnAction(event -> {
            if (packageTypeComboBox.getSelectionModel().isEmpty())
                mfv.toggleOff(2);
            else
                mfv.toggleOn(2);
        });
    }

    private void initDestinationComboBoxOnSelect() {
        officeComboBox.setOnAction(event -> {
            if (isDestinationOfficeAddress()) {
                if (packageTypeComboBox.getSelectionModel().isEmpty())
                    mfv.toggleOff(3);
                else
                    mfv.toggleOn(3);
            }
        });
    }

    private Client getSender() {
        return senderComboBox.getValue();
    }

    private Client getReceiver() {
        return receiverComboBox.getValue();
    }

    private String getPackageType() {
        return packageTypeComboBox.getValue();
    }

    private Address getDestinationAddress() {
        return (isDestinationOfficeAddress()) ? officeComboBox.getValue().getAddress() : getReceiver().getAddress();
    }

    private boolean isDestinationOfficeAddress() {
        return officeDestinationCheckBox.isSelected();
    }

    private void initOfficeSelectionToggle() {
        officeDestinationCheckBox.setOnAction(event -> officeComboBox.setVisible(!officeComboBox.isVisible()));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        senderComboBox.setItems(clients);
        receiverComboBox.setItems(clients);
        officeComboBox.setItems(offices);
        packageTypeComboBox.getItems().addAll(PackageType.getAll());
        initSenderComboBoxOnSelect();
        initReceiverComboBoxOnSelect();
        initTypeComboBoxOnSelect();
        initDestinationComboBoxOnSelect();
        initOfficeSelectionToggle();
        initRegisterPackageButton();
        initUpdateScheduler();
    }

    private void initUpdateScheduler() {
        updateService = new ScheduledService<Void>() {
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    protected Void call() {
                        List<Client> fetchedClients = DatabasePoller.fetchClientUpdates(clients);
                        List<Office> fetchedOffices = DatabasePoller.fetchOfficeUpdates(offices, CourierController.loggedUser.getOffice().getCompany());
                        Platform.runLater(() -> update(fetchedClients, fetchedOffices));
                        return null;
                    }
                };
            }
        };
        updateService.setPeriod(Duration.seconds(5));
        updateService.start();
    }

    public void update(List<Client> updatedClients, List<Office> updatedOffices) {
        if (senderComboBox.getSelectionModel().isEmpty() && receiverComboBox.getSelectionModel().isEmpty()) {
            clients.addAll(updatedClients);
        }
        if (officeComboBox.getSelectionModel().isEmpty()) {
            offices.addAll(updatedOffices);
        }
    }

    private Package buildPackage() {
        Package pack = new Package();
        pack.setSender(getSender());
        pack.setReceiver(getReceiver());
        pack.setReceiverAddress(getDestinationAddress());
        pack.setType(getPackageType());
        pack.setPrice(PriceCalculator.calculatePrice(pack.getType(), isDestinationOfficeAddress()));
        pack.setCreationDate(CurrentTimestamp.get());
        pack.setOffice(CourierController.loggedUser.getOffice());
        pack.setStatus(PackageStatus.REGISTERED);
        return pack;
    }

    private void initRegisterPackageButton() {
        registerPackageButton.setOnAction(event -> {
            if (!isDestinationOfficeAddress())
                mfv.toggleOn(3);
            if (mfv.isInvalidObject()) {
                WarningAlert.showAndWait("Please make sure to input correct data in all fields!");
                logger.warn(WarningAlert.getContentText());
            } else {
                Package pack = buildPackage();
                long id = packageService.save(pack);
                if (id == -1) {
                    ErrorAlert.showAndWait("An error occurred while trying to create a package!");
                    logger.error(ErrorAlert.getContentText());
                } else {
                    InfoAlert.setHeaderText("Total cost: " + pack.getPrice());
                    InfoAlert.showAndWait("Package successfully created!");
                    InfoAlert.setHeaderText(null);
                    logger.info(InfoAlert.getContentText());
                    clearComponents();
                }
            }
        });
    }

    private void clearComponents() {
        officeComboBox.getSelectionModel().clearSelection();
        receiverComboBox.getSelectionModel().clearSelection();
        senderComboBox.getSelectionModel().clearSelection();
        packageTypeComboBox.getSelectionModel().clearSelection();
        officeDestinationCheckBox.setSelected(false);
        officeComboBox.setVisible(false);
        mfv.reset();
    }
}
