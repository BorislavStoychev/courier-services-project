package com.courierservices.presenter.controller.subcontroller;

import com.courierservices.model.pojo.Address;
import com.courierservices.model.pojo.Company;
import com.courierservices.model.pojo.Warehouse;
import com.courierservices.presenter.service.CompanyService;
import com.courierservices.presenter.util.CurrentTimestamp;
import com.courierservices.presenter.util.FieldValidator;
import com.courierservices.presenter.util.MultipleFieldValidator;
import com.courierservices.presenter.util.StyleConstants;
import com.courierservices.presenter.util.alerts.ErrorAlert;
import com.courierservices.presenter.util.alerts.InfoAlert;
import com.courierservices.presenter.util.alerts.WarningAlert;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author Yordan Marinov
 */
public final class CompanyCreateController implements Initializable {
    @FXML
    private Button createButton;
    @FXML
    private TextField companyNameField;
    @FXML
    private TextField warehouseStreetField;
    @FXML
    private TextField warehouseCityField;
    @FXML
    private TextField warehouseCountryField;
    @FXML
    private TextField warehousePostalCodeField;

    private CompanyService companyService = new CompanyService();
    private MultipleFieldValidator mfv = new MultipleFieldValidator(5);
    private static final Logger logger
            = LoggerFactory.getLogger(CompanyCreateController.class);

    private Address buildAddress() {
        Address address = new Address();
        address.setStreet(warehouseStreetField.getText());
        address.setCity(warehouseCityField.getText());
        address.setCountry(warehouseCountryField.getText());
        address.setPostalCode(Integer.parseInt(warehousePostalCodeField.getText()));
        return address;
    }

    private Warehouse buildWarehouse(Address warehouseAddress, Company company) {
        Warehouse warehouse = new Warehouse();
        warehouse.setAddress(warehouseAddress);
        warehouse.setCompany(company);
        return warehouse;
    }

    private Company buildCompany() {
        Company company = new Company();
        Address address = buildAddress();
        Warehouse warehouse = buildWarehouse(address, company);
        company.setName(companyNameField.getText());
        company.setCreationDate(CurrentTimestamp.get());
        company.setWarehouse(warehouse);
        return company;
    }

    private void initCreateCompanyButton() {
        createButton.setOnAction(event -> {
            if (mfv.isInvalidObject()) {
                WarningAlert.showAndWait("Please make sure to input correct data in all fields!");
                logger.warn(WarningAlert.getContentText());
            } else {
                Company company = buildCompany();
                long id = companyService.save(company);
                if (id == -1) {
                    ErrorAlert.showAndWait("An error occurred while trying to create a company!");
                    logger.error(ErrorAlert.getContentText());
                } else {
                    InfoAlert.showAndWait("Company successfully created!");
                    logger.info(InfoAlert.getContentText());
                    clearComponents();
                }
            }
        });
    }

    private void clearComponents() {
        companyNameField.clear();
        warehouseStreetField.clear();
        warehouseCityField.clear();
        warehouseCountryField.clear();
        warehousePostalCodeField.clear();
        mfv.reset();
    }

    private void initCompanyNameListener() {
        companyNameField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                companyNameField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(0);
            } else {
                companyNameField.setStyle(StyleConstants.validField);
                mfv.toggleOn(0);
            }
        });
    }

    private void initWarehouseStreetListener() {
        warehouseStreetField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidStreet(newValue)) {
                warehouseStreetField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(1);
            } else {
                warehouseStreetField.setStyle(StyleConstants.validField);
                mfv.toggleOn(1);
            }
        });
    }

    private void initWarehouseCityListener() {
        warehouseCityField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                warehouseCityField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(2);
            } else {
                warehouseCityField.setStyle(StyleConstants.validField);
                mfv.toggleOn(2);
            }
        });
    }

    private void initWarehouseCountryListener() {
        warehouseCountryField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                warehouseCountryField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(3);
            } else {
                warehouseCountryField.setStyle(StyleConstants.validField);
                mfv.toggleOn(3);
            }
        });
    }

    private void initWarehousePostalCodeListener() {
        warehousePostalCodeField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidPostCode(newValue)) {
                warehousePostalCodeField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(4);
            } else {
                warehousePostalCodeField.setStyle(StyleConstants.validField);
                mfv.toggleOn(4);
            }
        });
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initCompanyNameListener();
        initWarehouseStreetListener();
        initWarehouseCityListener();
        initWarehouseCountryListener();
        initWarehousePostalCodeListener();
        initCreateCompanyButton();
    }
}
