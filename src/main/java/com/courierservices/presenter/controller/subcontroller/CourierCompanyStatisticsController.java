package com.courierservices.presenter.controller.subcontroller;

import com.courierservices.model.pojo.Courier;
import com.courierservices.model.pojo.Shipment;
import com.courierservices.presenter.controller.CourierController;
import com.courierservices.presenter.service.CourierService;
import com.courierservices.presenter.service.ShipmentService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;

import java.net.URL;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

public final class CourierCompanyStatisticsController implements Initializable {
    @FXML
    private Button showButton;
    @FXML
    private PieChart courierCompanyClientsPieChart;
    @FXML
    private DatePicker fromDate;
    @FXML
    private DatePicker toDate;

    private final CourierService courierService = new CourierService();
    private final ShipmentService shipmentService = new ShipmentService();
    private final ObservableList<PieChart.Data> data = FXCollections.observableArrayList(new ArrayList<>());
    private final Map<PieChart.Data, String> map = new HashMap<>();
    private Timestamp from;
    private Timestamp to;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        courierCompanyClientsPieChart.setData(data);
        initShowButton();
    }

    private boolean filterShipments(Shipment shipment) {
        return shipment.getDispatchTime().after(from) && shipment.getDispatchTime().before(to);
    }

    private boolean filterCouriers(Courier courier) {
        return courier.getCreationDate().after(from) && courier.getCreationDate().before(to) &&
                courier.getOffice().getCompany().getCompanyId() == CourierController.loggedUser.getOffice().getCompany().getCompanyId();
    }

    private PieChart.Data buildSlice(Courier courier) {
        String courierName = courier.getFirstName() + " " + courier.getLastName();
        int shipmentsNumber = (int) shipmentService.getAll(courier).stream().filter(this::filterShipments).count();
        return new PieChart.Data(courierName, shipmentsNumber);
    }


    private void constructDiagram(List<Courier> couriers) {
        if (couriers != null) {
            List<Courier> existingCouriersInCompany = couriers.stream().filter(this::filterCouriers).collect(Collectors.toList());
            existingCouriersInCompany.forEach(courier ->
            {
                PieChart.Data slice = buildSlice(courier);
                data.add(slice);
                map.put(slice, slice.getName());
            });
            initValuesOnHover();
        }
    }

    private void initShowButton() {
        showButton.setOnAction(event ->
        {
            courierCompanyClientsPieChart.getData().clear();
            data.clear();
            map.clear();
            if (fromDate.getValue() != null && toDate.getValue() != null) {
                from = Timestamp.valueOf(fromDate.getValue().atStartOfDay());
                to = Timestamp.valueOf(toDate.getValue().atStartOfDay());
                List<Courier> couriers = courierService.getAllActive();
                constructDiagram(couriers);
                fromDate.setValue(null);
                toDate.setValue(null);
            }
        });
    }

    private void initValuesOnHover() {
        data.forEach(slice -> slice.getNode().setOnMouseEntered(event -> {
            slice.setName(String.valueOf((int) (slice.getPieValue())));
        }));
        data.forEach(slice -> slice.getNode().setOnMouseExited(event -> {
            slice.setName(map.get(slice));
        }));
    }
}
