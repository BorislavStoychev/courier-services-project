package com.courierservices.presenter.controller.subcontroller;

import com.courierservices.model.pojo.Address;
import com.courierservices.model.pojo.Client;
import com.courierservices.model.pojo.Credentials;
import com.courierservices.presenter.controller.CourierController;
import com.courierservices.presenter.service.ClientService;
import com.courierservices.presenter.service.CredentialsService;
import com.courierservices.presenter.util.*;
import com.courierservices.presenter.util.alerts.ErrorAlert;
import com.courierservices.presenter.util.alerts.InfoAlert;
import com.courierservices.presenter.util.alerts.WarningAlert;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author Yordan Marinov
 */
public final class ClientCreateController implements Initializable {
    @FXML
    private Button createButton;
    @FXML
    private Button generateCredentialsButton;
    @FXML
    private TextField usernameField;
    @FXML
    private TextField passwordField;
    @FXML
    private TextField firstNameField;
    @FXML
    private TextField middleNameField;
    @FXML
    private TextField lastNameField;
    @FXML
    private TextField phoneNumberField;
    @FXML
    private TextField emailAddressField;
    @FXML
    private TextField streetField;
    @FXML
    private TextField cityField;
    @FXML
    private TextField countryField;
    @FXML
    private TextField postalCodeField;

    private ClientService clientService = new ClientService();
    private CredentialsService credentialsService = new CredentialsService();
    private MultipleFieldValidator mfv = new MultipleFieldValidator(9);
    private static final Logger logger
            = LoggerFactory.getLogger(ClientCreateController.class);

    private void initGenerateCredentialsButton() {
        generateCredentialsButton.setOnAction(event -> {
            usernameField.setText(UsernameGenerator.generateClientUsername());
            passwordField.setText(PasswordGenerator.generatePassword());
            createButton.setDisable(false);
        });
    }

    private void initClientFirstNameListener() {
        firstNameField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                firstNameField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(0);
            } else {
                firstNameField.setStyle(StyleConstants.validField);
                mfv.toggleOn(0);
            }
        });
    }

    private void initClientMiddleNameListener() {
        middleNameField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                middleNameField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(1);
            } else {
                middleNameField.setStyle(StyleConstants.validField);
                mfv.toggleOn(1);
            }
        });
    }

    private void initClientLastNameListener() {
        lastNameField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                lastNameField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(2);
            } else {
                lastNameField.setStyle(StyleConstants.validField);
                mfv.toggleOn(2);
            }
        });
    }

    private void initClientPhoneNumberListener() {
        phoneNumberField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidPhoneNumber(newValue)) {
                phoneNumberField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(3);
            } else {
                phoneNumberField.setStyle(StyleConstants.validField);
                mfv.toggleOn(3);
            }
        });
    }

    private void initClientEmailAddressListener() {
        emailAddressField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidEmailAddress(newValue)) {
                emailAddressField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(4);
            } else {
                emailAddressField.setStyle(StyleConstants.validField);
                mfv.toggleOn(4);
            }
        });
    }

    private void initClientStreetListener() {
        streetField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidStreet(newValue)) {
                streetField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(5);
            } else {
                streetField.setStyle(StyleConstants.validField);
                mfv.toggleOn(5);
            }
        });
    }

    private void initClientCityListener() {
        cityField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                cityField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(6);
            } else {
                cityField.setStyle(StyleConstants.validField);
                mfv.toggleOn(6);
            }
        });
    }

    private void initClientCountryListener() {
        countryField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                countryField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(7);
            } else {
                countryField.setStyle(StyleConstants.validField);
                mfv.toggleOn(7);
            }
        });
    }

    private void initClientPostalCodeListener() {
        postalCodeField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidPostCode(newValue)) {
                postalCodeField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(8);
            } else {
                postalCodeField.setStyle(StyleConstants.validField);
                mfv.toggleOn(8);
            }
        });
    }

    private void clearComponents() {
        passwordField.clear();
        firstNameField.clear();
        middleNameField.clear();
        lastNameField.clear();
        phoneNumberField.clear();
        emailAddressField.clear();
        streetField.clear();
        cityField.clear();
        countryField.clear();
        postalCodeField.clear();
        mfv.reset();
    }

    private Address buildAddress() {
        Address address = new Address();
        address.setStreet(streetField.getText());
        address.setCity(cityField.getText());
        address.setCountry(countryField.getText());
        address.setPostalCode(Integer.parseInt(postalCodeField.getText()));
        return address;
    }

    private Client buildClient() {
        Client client = new Client();
        client.setFirstName(firstNameField.getText());
        client.setMiddleName(middleNameField.getText());
        client.setLastName(lastNameField.getText());
        client.setPhoneNumber(phoneNumberField.getText());
        client.setEmailAddress(emailAddressField.getText());
        client.setAddress(buildAddress());
        client.setCreationDate(CurrentTimestamp.get());
        client.setCompany(CourierController.loggedUser.getOffice().getCompany());
        return client;
    }

    private Credentials buildCredentials(long id) {
        Credentials credentials = new Credentials();
        credentials.setUsername(usernameField.getText());
        credentials.setPassword(passwordField.getText());
        credentials.setUserId(id);
        return credentials;
    }

    private void initCreateClientButton() {
        createButton.setOnAction(event -> {
            if (mfv.isInvalidObject()) {
                WarningAlert.showAndWait("Please make sure to input correct data in all fields.");
                logger.warn(WarningAlert.getContentText());
            } else {
                Client client = buildClient();
                long clientId = clientService.save(client);
                if (clientId == -1) {
                    ErrorAlert.showAndWait("An error occurred while trying to create a client!");
                    logger.error(ErrorAlert.getContentText());
                } else {
                    credentialsService.save(buildCredentials(clientId));
                    InfoAlert.showAndWait("Client successfully created.");
                    logger.info(InfoAlert.getContentText());
                    clearComponents();
                }
            }
        });
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initClientFirstNameListener();
        initClientMiddleNameListener();
        initClientLastNameListener();
        initClientPhoneNumberListener();
        initClientEmailAddressListener();
        initClientCityListener();
        initClientCountryListener();
        initClientStreetListener();
        initClientPostalCodeListener();
        initGenerateCredentialsButton();
        initCreateClientButton();
    }
}
