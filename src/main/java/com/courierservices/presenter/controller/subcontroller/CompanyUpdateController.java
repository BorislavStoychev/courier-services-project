package com.courierservices.presenter.controller.subcontroller;

import com.courierservices.model.pojo.Address;
import com.courierservices.model.pojo.Company;
import com.courierservices.presenter.service.CompanyService;
import com.courierservices.presenter.util.DatabasePoller;
import com.courierservices.presenter.util.FieldValidator;
import com.courierservices.presenter.util.MultipleFieldValidator;
import com.courierservices.presenter.util.StyleConstants;
import com.courierservices.presenter.util.alerts.ErrorAlert;
import com.courierservices.presenter.util.alerts.InfoAlert;
import com.courierservices.presenter.util.alerts.WarningAlert;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author Yordan Marinov
 */
public final class CompanyUpdateController implements Initializable {
    @FXML
    private ComboBox<Company> companyComboBox;
    @FXML
    private TextField companyNameField;
    @FXML
    private TextField warehouseStreetField;
    @FXML
    private TextField warehouseCityField;
    @FXML
    private TextField warehouseCountryField;
    @FXML
    private TextField warehousePostalCodeField;
    @FXML
    private Button updateButton;
    @FXML
    private CheckBox closedDownCheckBox;

    private static CompanyService companyService = new CompanyService();
    private static ObservableList<Company> companies;
    private MultipleFieldValidator mfv = new MultipleFieldValidator(5);
    private static ScheduledService<Void> updateService;
    private static final Logger logger
            = LoggerFactory.getLogger(CompanyUpdateController.class);

    private void initUpdateCompanyButton() {
        updateButton.setOnAction(event -> {
            if (mfv.isInvalidObject()) {
                WarningAlert.showAndWait("Please make sure to input correct data in all fields!");
                logger.warn(WarningAlert.getContentText());
            } else {
                Company selectedCompany = companyComboBox.getSelectionModel().getSelectedItem();
                updateCompany(selectedCompany);
                int id = companyService.update(selectedCompany);
                companies.remove(companyComboBox.getSelectionModel().getSelectedItem());
                if (id == -1) {
                    ErrorAlert.showAndWait("An error occurred while trying to update a company!");
                    logger.error(ErrorAlert.getContentText());
                } else {
                    InfoAlert.showAndWait("Company successfully updated!");
                    logger.info(InfoAlert.getContentText());
                    clearComponents();
                    disableComponents();
                }
            }
        });
    }

    private void updateAddress(Address address) {
        address.setStreet(warehouseStreetField.getText());
        address.setCity(warehouseCityField.getText());
        address.setCountry(warehouseCountryField.getText());
        address.setPostalCode(Integer.parseInt(warehousePostalCodeField.getText()));
    }

    private void updateCompany(Company company) {
        updateAddress(company.getWarehouse().getAddress());
        company.setName(companyNameField.getText());
        company.setInactiveStatus(closedDownCheckBox.isSelected());
    }

    private void clearComponents() {
        companyNameField.clear();
        warehouseStreetField.clear();
        warehouseCityField.clear();
        warehouseCountryField.clear();
        warehousePostalCodeField.clear();
        companyComboBox.getSelectionModel().clearSelection();
        mfv.reset();
    }

    private void initComboBoxOnSelect() {
        companyComboBox.setOnAction(event -> {
            if (updateButton.isDisabled())
                enableComponents();
            loadFields(companyComboBox.getValue());
        });
    }

    private void loadFields(Company company) {
        if (company != null) {
            companyNameField.setText(company.getName());
            closedDownCheckBox.setSelected(company.isInactiveStatus());
            Address warehouseAddress = company.getWarehouse().getAddress();
            warehouseCityField.setText(warehouseAddress.getCity());
            warehouseCountryField.setText(warehouseAddress.getCountry());
            warehouseStreetField.setText(warehouseAddress.getStreet());
            warehousePostalCodeField.setText(String.valueOf(warehouseAddress.getPostalCode()));
        }
    }

    private void initCompanyNameListener() {
        companyNameField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                companyNameField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(0);
            } else {
                companyNameField.setStyle(StyleConstants.validField);
                mfv.toggleOn(0);
            }
        });
    }

    private void initWarehouseStreetListener() {
        warehouseStreetField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidStreet(newValue)) {
                warehouseStreetField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(1);
            } else {
                warehouseStreetField.setStyle(StyleConstants.validField);
                mfv.toggleOn(1);
            }
        });
    }

    private void initWarehouseCityListener() {
        warehouseCityField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                warehouseCityField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(2);
            } else {
                warehouseCityField.setStyle(StyleConstants.validField);
                mfv.toggleOn(2);
            }
        });
    }

    private void initWarehouseCountryListener() {
        warehouseCountryField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidText(newValue)) {
                warehouseCountryField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(3);
            } else {
                warehouseCountryField.setStyle(StyleConstants.validField);
                mfv.toggleOn(3);
            }
        });
    }

    private void initWarehousePostalCodeListener() {
        warehousePostalCodeField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!FieldValidator.isValidPostCode(newValue)) {
                warehousePostalCodeField.setStyle(StyleConstants.invalidField);
                mfv.toggleOff(4);
            } else {
                warehousePostalCodeField.setStyle(StyleConstants.validField);
                mfv.toggleOn(4);
            }
        });
    }

    private void initUpdateScheduler() {
        updateService = new ScheduledService<Void>() {
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    protected Void call() {
                        List<Company> fetchedCompanies = DatabasePoller.fetchCompanyUpdates(companies, false);
                        Platform.runLater(() -> update(fetchedCompanies));
                        return null;
                    }
                };
            }
        };
        updateService.setPeriod(Duration.seconds(5));
        updateService.start();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        companies = FXCollections.observableArrayList(companyService.getAll());
        companyComboBox.setItems(companies);
        initCompanyNameListener();
        initWarehouseStreetListener();
        initWarehouseCityListener();
        initWarehouseCountryListener();
        initWarehousePostalCodeListener();
        initComboBoxOnSelect();
        initUpdateCompanyButton();
        initUpdateScheduler();
    }

    private void enableComponents() {
        companyNameField.setDisable(false);
        warehouseStreetField.setDisable(false);
        warehouseCityField.setDisable(false);
        warehouseCountryField.setDisable(false);
        warehousePostalCodeField.setDisable(false);
        closedDownCheckBox.setDisable(false);
        updateButton.setDisable(false);
    }

    private void disableComponents() {
        companyNameField.setDisable(true);
        warehouseStreetField.setDisable(true);
        warehouseCityField.setDisable(true);
        warehouseCountryField.setDisable(true);
        warehousePostalCodeField.setDisable(true);
        closedDownCheckBox.setDisable(true);
        updateButton.setDisable(true);
    }

    public void update(List<Company> updatedCompanies) {
        if (companyComboBox.getSelectionModel().getSelectedItem() == null) {
            companies.addAll(updatedCompanies);
        }
    }

    public static void stopUpdate() {
        if (updateService != null)
            updateService.cancel();
    }
}
