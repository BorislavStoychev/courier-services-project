package com.courierservices.presenter.controller.subcontroller;

import com.courierservices.model.pojo.Courier;
import com.courierservices.model.pojo.Shipment;
import com.courierservices.presenter.service.CourierService;
import com.courierservices.presenter.service.ShipmentService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;

import java.net.URL;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Yordan Marinov
 */
public final class CourierWorkStatisticsController implements Initializable {
    @FXML
    private Button showButton;
    @FXML
    private PieChart courierWorkPieChart;
    @FXML
    private DatePicker fromDate;
    @FXML
    private DatePicker toDate;

    private final ShipmentService shipmentService = new ShipmentService();
    private final CourierService courierService = new CourierService();
    private final ObservableList<PieChart.Data> data = FXCollections.observableArrayList(new ArrayList<>());
    private final Map<PieChart.Data, String> map = new HashMap<>();
    private Timestamp from;
    private Timestamp to;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        courierWorkPieChart.setData(data);
        initShowButton();
    }

    private boolean filterShipments(Shipment shipment) {
        return shipment.getDispatchTime().after(from) && shipment.getDispatchTime().before(to);
    }

    private PieChart.Data buildSlice(Courier courier) {
        String courierName = courier.getFirstName() + " " + courier.getLastName();
        int shipmentsNumber = (int) shipmentService.getAll(courier).stream().filter(this::filterShipments).count();
        return new PieChart.Data(courierName, shipmentsNumber);
    }

    private void initShowButton() {
        showButton.setOnAction(event -> {
            courierWorkPieChart.getData().clear();
            data.clear();
            map.clear();
            if (fromDate.getValue() != null && toDate.getValue() != null) {
                from = Timestamp.valueOf(fromDate.getValue().atStartOfDay());
                to = Timestamp.valueOf(toDate.getValue().atStartOfDay());
                List<Courier> couriers = courierService.getAllActive();
                constructDiagram(couriers);
                fromDate.setValue(null);
                toDate.setValue(null);
            }
        });
    }

    private void constructDiagram(List<Courier> couriers) {
        if (couriers != null) {
            List<Courier> existingCouriers = couriers.stream().filter(courier ->
                    courier.getCreationDate().before(to)).collect(Collectors.toList());
            existingCouriers.forEach(courier -> {
                PieChart.Data slice = buildSlice(courier);
                if (slice.getPieValue() > 0)
                    data.add(slice);
                map.put(slice, slice.getName());
            });
            initValuesOnHover();
        }
    }

    private void initValuesOnHover() {
        data.forEach(slice -> slice.getNode().setOnMouseEntered(event -> {
            slice.setName(String.valueOf((int) (slice.getPieValue())));
        }));
        data.forEach(slice -> slice.getNode().setOnMouseExited(event -> {
            slice.setName(map.get(slice));
        }));
    }
}
