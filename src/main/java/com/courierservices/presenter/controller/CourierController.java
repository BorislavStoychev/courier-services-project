package com.courierservices.presenter.controller;

import com.courierservices.model.pojo.Courier;
import com.courierservices.model.pojo.User;
import com.courierservices.presenter.controller.subcontroller.PackageCreateController;
import com.courierservices.presenter.controller.subcontroller.PackageViewController;
import com.courierservices.presenter.controller.subcontroller.ShipmentCreateController;
import com.courierservices.presenter.util.ControllerLoader;
import com.courierservices.view.ViewManager;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author Yordan Marinov
 */
public final class CourierController extends ControllerLoader implements Initializable, Controller {
    public static Courier loggedUser;
    @FXML
    private MenuItem clientRegister;
    @FXML
    private MenuItem packageRegister;
    @FXML
    private MenuItem shipmentForm;
    @FXML
    private MenuItem packagesView;
    @FXML
    private MenuItem courierCompanyStatisticsSubmenu;
    @FXML
    private Button logoutButton;
    @FXML
    private Pane mainPane;

    private Pane activePane = null;

    private void initClientRegisterMenuItem() {
        clientRegister.setOnAction(event -> {
            String controllerName = "client_create";
            if (!panes.containsKey(controllerName))
                loadController("fxml/client_create.fxml");
            setPane(controllerName);
        });
    }

    private void initPackageRegisterMenuItem() {
        packageRegister.setOnAction(event -> {
            String controllerName = "package_create";
            if (!panes.containsKey(controllerName))
                loadController("fxml/package_create.fxml");
            setPane(controllerName);
        });
    }

    private void initShipmentFormMenuItem() {
        shipmentForm.setOnAction(event -> {
            String controllerName = "shipment_create";
            if (!panes.containsKey(controllerName))
                loadController("fxml/shipment_create.fxml");
            setPane(controllerName);
        });
    }

    private void initPackagesViewMenuItem() {
        packagesView.setOnAction(event -> {
            String controllerName = "package_view";
            if (!panes.containsKey(controllerName))
                loadController("fxml/package_view.fxml");
            setPane(controllerName);
        });
    }

    private void initCourierCompanyStatisticsMenuItem() {
        courierCompanyStatisticsSubmenu.setOnAction(event -> {
            String controllerName = "courier_company_statistics";
            if (!panes.containsKey(controllerName))
                loadController("fxml/courier_company_statistics.fxml");
            setPane(controllerName);
        });
    }

    private void initLogoutButton() {
        logoutButton.setOnAction(event -> {
            stopUpdateServices();
            mainPane.getChildren().remove(activePane);
            activePane = null;
            ViewManager.resetScene();
        });
    }

    private void setPane(String pane) {
        mainPane.getChildren().remove(activePane);
        activePane = panes.get(pane);
        mainPane.getChildren().add(activePane);
    }

    private void stopUpdateServices() {
        PackageCreateController.stopUpdate();
        PackageViewController.stopUpdate();
        ShipmentCreateController.stopUpdate();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initClientRegisterMenuItem();
        initPackageRegisterMenuItem();
        initShipmentFormMenuItem();
        initPackagesViewMenuItem();
        initCourierCompanyStatisticsMenuItem();
        initLogoutButton();
    }

    @Override
    public void setLoggedUser(User user) {
        loggedUser = (Courier) user;
    }
}
