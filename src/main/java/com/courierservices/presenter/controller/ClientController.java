package com.courierservices.presenter.controller;

import com.courierservices.model.pojo.Package;
import com.courierservices.model.pojo.*;
import com.courierservices.presenter.service.PackageService;
import com.courierservices.presenter.util.alerts.InfoAlert;
import com.courierservices.view.ViewManager;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import javafx.util.Duration;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author Borislav Stoychev
 */

public final class ClientController implements Controller, Initializable {
    private static Client loggedUser;
    @FXML
    private Button logoutButton;
    @FXML
    private TableView<Package> packagesTable;
    @FXML
    private TableColumn<Package, Client> sender;
    @FXML
    private TableColumn<Package, Client> receiver;
    @FXML
    private TableColumn<Package, PackageStatus> status;
    @FXML
    private TableColumn<Package, String> type;
    @FXML
    private TableColumn<Package, String> cancelAction;
    @FXML
    private TableColumn<Package, String> receiveAction;

    private ObservableList<Package> packages = FXCollections.observableArrayList(new ArrayList<>());
    private final PackageService packageService = new PackageService();
    private ScheduledService<Void> updateService;

    @Override
    public void setLoggedUser(User user) {
        loggedUser = (Client) user;
    }

    private void displayCancelNotification(Package pack) {
        pack.setReceiverAddress(pack.getOffice().getCompany().getWarehouse().getAddress());
        InfoAlert.showAndWait("You have cancelled your package successfully.");
        packages.remove(pack);
        pack.setStatus(PackageStatus.CANCELLED);
        packageService.update(pack);
    }

    private void displayDeliveredNotification(Package pack) {
        InfoAlert.showAndWait("Package " + pack + "has been delivered!");
        packages.remove(pack);
        pack.setStatus(PackageStatus.DELIVERED);
        packageService.update(pack);
    }

    private void displayReceivedNotification(Package pack) {
        InfoAlert.showAndWait("Package marked as received.");
        packages.remove(pack);
        pack.setStatus(PackageStatus.RECEIVED);
        packageService.update(pack);
    }

    private Callback<TableColumn<Package, String>, TableCell<Package, String>> createCancelButtonCell() {
        return new Callback<TableColumn<Package, String>, TableCell<Package, String>>() {
            @Override
            public TableCell<Package, String> call(final TableColumn<Package, String> param) {
                return new TableCell<Package, String>() {
                    final Button button = new Button("Cancel");

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            if (getTableView().getItems().get(getIndex()).getStatus().equals(PackageStatus.INTRANSIT)) {
                                button.setOnAction(event -> {
                                    Package pack = getTableView().getItems().get(getIndex());
                                    displayCancelNotification(pack);
                                });
                                setGraphic(button);
                                setText(null);
                            }

                        }
                    }
                };
            }
        };
    }

    private Callback<TableColumn<Package, String>, TableCell<Package, String>> createReceiveButtonCell() {
        return new Callback<TableColumn<Package, String>, TableCell<Package, String>>() {
            @Override
            public TableCell<Package, String> call(final TableColumn<Package, String> param) {
                return new TableCell<Package, String>() {
                    final Button btn = new Button("Received");

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            if (getTableView().getItems().get(getIndex()).getStatus().equals(PackageStatus.DELIVERED)) {
                                btn.setOnAction(event -> {
                                    Package pack = getTableView().getItems().get(getIndex());
                                    displayReceivedNotification(pack);
                                });
                                setGraphic(btn);
                                setText(null);
                            }
                        }
                    }
                };
            }
        };
    }

    private void initCellProperties() {
        sender.setCellValueFactory(new PropertyValueFactory<>("sender"));
        receiver.setCellValueFactory(new PropertyValueFactory<>("receiver"));
        status.setCellValueFactory(new PropertyValueFactory<>("status"));
        type.setCellValueFactory(new PropertyValueFactory<>("type"));
        cancelAction.setCellValueFactory(new PropertyValueFactory<>("cancelAction"));
        receiveAction.setCellValueFactory(new PropertyValueFactory<>("receiveAction"));
        cancelAction.setCellFactory(createCancelButtonCell());
        receiveAction.setCellFactory(createReceiveButtonCell());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        packagesTable.setItems(packages);
        initLogoutButton();
        initUpdateScheduler();
        initCellProperties();
    }

    private void initUpdateScheduler() {
        updateService = new ScheduledService<Void>() {
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    protected Void call() {
                        List<Package> updatedPackages = packageService.getClientPackages(loggedUser);
                        List<Package> updatedDeliveredPackages = packageService.getDeliveredPackages(ClientController.loggedUser);
                        Callback<TableColumn<Package, String>, TableCell<Package, String>> cancelButtonCell = createCancelButtonCell();
                        Callback<TableColumn<Package, String>, TableCell<Package, String>> receiveButtonCell = createReceiveButtonCell();
                        Platform.runLater(() -> {
                            update(updatedPackages, updatedDeliveredPackages);
                            cancelAction.setCellFactory(cancelButtonCell);
                            receiveAction.setCellFactory(receiveButtonCell);
                        });
                        return null;
                    }
                };
            }
        };
        updateService.setPeriod(Duration.seconds(5));
        updateService.start();
    }

    private void update(List<Package> updatedPackages, List<Package> updatedDeliveredPackages) {
        packages.clear();
        packages.addAll(updatedPackages);
        if (updatedDeliveredPackages != null)
            updatedDeliveredPackages.forEach(this::displayDeliveredNotification);
    }

    private void stopUpdate() {
        if (updateService != null)
            updateService.cancel();
    }

    private void initLogoutButton() {
        logoutButton.setOnAction(event -> {
            stopUpdate();
            ViewManager.resetScene();
        });
    }
}
