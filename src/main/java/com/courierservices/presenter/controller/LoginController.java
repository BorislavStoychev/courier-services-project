package com.courierservices.presenter.controller;

import com.courierservices.model.pojo.Client;
import com.courierservices.model.pojo.Courier;
import com.courierservices.model.pojo.Credentials;
import com.courierservices.model.pojo.User;
import com.courierservices.presenter.service.ClientService;
import com.courierservices.presenter.service.CourierService;
import com.courierservices.presenter.service.CredentialsService;
import com.courierservices.presenter.util.alerts.ErrorAlert;
import com.courierservices.presenter.util.alerts.WarningAlert;
import com.courierservices.view.ViewManager;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author Yordan Marinov
 */
public final class LoginController implements Initializable {

    @FXML
    public PasswordField passwordField;
    @FXML
    public TextField usernameField;
    @FXML
    public Button loginButton;

    private CredentialsService credentialsService = new CredentialsService();
    private static final Logger logger
            = LoggerFactory.getLogger(LoginController.class);

    private void initLoginButton() {
        loginButton.setOnAction(event -> {
            String username = getUsername();
            String password = getPassword();
            if (username.equals("admin") && password.equals("admin")) {
                loadAdministratorController();
                return;
            }
            Credentials loginCredentials = credentialsService.get(username);
            if (loginCredentials == null) {
                WarningAlert.showAndWait("The username you have entered is invalid.");
                logger.warn(WarningAlert.getContentText());
                return;
            }
            if (!password.equals(loginCredentials.getPassword())) {
                WarningAlert.showAndWait("The password you have entered is invalid.");
                logger.warn(WarningAlert.getContentText());
                return;
            }
            User loggedUser = getUser(loginCredentials.getUsername(), loginCredentials.getUserId());
            if (loggedUser == null) {
                ErrorAlert.showAndWait("An error occurred while trying to log in.");
                logger.error(ErrorAlert.getContentText());
            } else {
                if (loggedUser.isInactive()) {
                    ErrorAlert.showAndWait("The user you are trying to log in is disabled.");
                    logger.warn(ErrorAlert.getContentText());
                    return;
                }
                loadController(loggedUser);
            }
        });
    }

    private void loadController(User user) {
        if (user instanceof Courier)
            loadCourierController(user);
        else if (user instanceof Client)
            loadClientController(user);
    }

    private void loadCourierController(User user) {
        URL url = getClass().getClassLoader().getResource("fxml/courier.fxml");
        if (url != null) {
            try {
                CourierController courierController = new CourierController();
                courierController.setLoggedUser(user);
                FXMLLoader loader = new FXMLLoader(url);
                loader.setController(courierController);
                if (ViewManager.activeScene == null)
                    ViewManager.activeScene = new Scene(loader.load());
                ViewManager.loadScene(ViewManager.activeScene);
            } catch (IOException ioException) {
                logger.error("Error while trying to load courier.fxml file.");
            }
        }
    }

    private void loadClientController(User user) {
        URL url = getClass().getClassLoader().getResource("fxml/client.fxml");
        if (url != null) {
            try {
                ClientController clientController = new ClientController();
                clientController.setLoggedUser(user);
                FXMLLoader loader = new FXMLLoader(url);
                loader.setController(clientController);
                if (ViewManager.activeScene == null)
                    ViewManager.activeScene = new Scene(loader.load());
                ViewManager.loadScene(ViewManager.activeScene);
            } catch (IOException ioException) {
                logger.error("Error while trying to load client.fxml file.");
            }
        }
    }

    private void loadAdministratorController() {
        URL url = getClass().getClassLoader().getResource("fxml/administrator.fxml");
        if (url != null) {
            try {
                if (ViewManager.activeScene == null)
                    ViewManager.activeScene = new Scene(FXMLLoader.load(url));
                ViewManager.loadScene(ViewManager.activeScene);
            } catch (IOException ioException) {
                logger.error("Error while trying to load administrator.fxml file.");
            }
        }
    }

    private String getUsername() {
        return usernameField.getText();
    }

    private String getPassword() {
        return passwordField.getText();
    }

    private User getUser(String username, long userId) {
        if (username.contains("cr_"))
            return new CourierService().get(userId);
        else if (username.contains("cl_"))
            return new ClientService().get(userId);
        return null;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initLoginButton();
    }
}
