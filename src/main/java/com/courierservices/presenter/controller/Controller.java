package com.courierservices.presenter.controller;

import com.courierservices.model.pojo.User;

/**
 * @author Yordan Marinov
 */
interface Controller {
    void setLoggedUser(User user);
}
