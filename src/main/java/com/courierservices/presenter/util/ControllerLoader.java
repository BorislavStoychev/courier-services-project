package com.courierservices.presenter.util;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Yordan Marinov
 */
public abstract class ControllerLoader {
    protected Map<String, Pane> panes = new HashMap<>();
    private static final Logger logger
            = LoggerFactory.getLogger(ControllerLoader.class);

    protected void loadController(String path) {
        URL url = getClass().getClassLoader().getResource(path);
        if (url != null) {
            try {
                panes.put(path.split("\\.")[0].split("/")[1], FXMLLoader.load(url));
            } catch (IOException ioException) {
                logger.error("Error while trying to load " + path + " file");
            }
        }
    }
}
