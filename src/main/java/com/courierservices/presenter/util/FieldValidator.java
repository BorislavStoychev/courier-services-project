package com.courierservices.presenter.util;

/**
 * @author Yordan Marinov
 */
public final class FieldValidator {
    public static boolean isValidText(String text) {
        return (text.matches("\\D{3,20}"));
    }

    public static boolean isValidPostCode(String postCode) {
        return (postCode.matches("\\d{4}"));
    }

    public static boolean isValidPhoneNumber(String phoneNumber) {
        return (phoneNumber.matches("\\b\\d{3,20}\\b"));
    }

    public static boolean isValidEmailAddress(String emailAddress) {
        return (emailAddress.matches("^[\\w-_.+]*[\\w-_.]@([\\w]+\\.)+[\\w]+[\\w]$"));
    }

    public static boolean isValidStreet(String street) {
        return (street.matches(
                "\\A(.*?)\\s+(\\d+[a-zA-Z]?\\s?[-]\\s?\\d*[a-zA-Z]?|\\d+[a-zA-Z-]?\\d*[a-zA-Z]?)"));
    }
}
