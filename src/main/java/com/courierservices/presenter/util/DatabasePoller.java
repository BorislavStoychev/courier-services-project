package com.courierservices.presenter.util;


import com.courierservices.model.pojo.Package;
import com.courierservices.model.pojo.*;
import com.courierservices.presenter.service.*;
import javafx.collections.ObservableList;

import java.util.List;

/**
 * @author Yordan Marinov
 */
public class DatabasePoller {
    private static CompanyService companyService = new CompanyService();
    private static OfficeService officeService = new OfficeService();
    private static CourierService courierService = new CourierService();
    private static ClientService clientService = new ClientService();
    private static PackageService packageService = new PackageService();

    public static List<Company> fetchCompanyUpdates(ObservableList<Company> currentCompanies, boolean fetchActive) {
        List<Company> fetchedCompanies = fetchActive ? companyService.getAllActive() : companyService.getAll();
        fetchedCompanies.removeAll(currentCompanies);
        return fetchedCompanies;
    }

    public static List<Office> fetchOfficeUpdates(ObservableList<Office> currentOffices, boolean fetchActive) {
        List<Office> fetchedOffices = fetchActive ? officeService.getAllActive() : officeService.getAll();
        fetchedOffices.removeAll(currentOffices);
        return fetchedOffices;
    }

    public static List<Office> fetchOfficeUpdates(ObservableList<Office> currentOffices, Company company) {
        List<Office> fetchedOffices = officeService.getAll(company);
        fetchedOffices.removeAll(currentOffices);
        return fetchedOffices;
    }

    public static List<Courier> fetchCourierUpdates(ObservableList<Courier> currentCouriers) {
        List<Courier> fetchedCouriers = courierService.getAll();
        fetchedCouriers.removeAll(currentCouriers);
        return fetchedCouriers;
    }

    public static List<Client> fetchClientUpdates(ObservableList<Client> currentClients) {
        List<Client> fetchedClients = clientService.getAll();
        fetchedClients.removeAll(currentClients);
        return fetchedClients;
    }

    public static List<Package> fetchPackageUpdates(ObservableList<Package> currentPackages, Office office) {
        List<Package> fetchedPackages = packageService.getAll(office);
        fetchedPackages.removeAll(currentPackages);
        return fetchedPackages;
    }

    public static List<Package> fetchPackageUpdates(ObservableList<Package> currentPackages, Office office, String status) {
        List<Package> fetchedPackages = packageService.getAll(office, status);
        fetchedPackages.removeAll(currentPackages);
        return fetchedPackages;
    }
}
