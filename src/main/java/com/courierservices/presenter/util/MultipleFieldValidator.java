package com.courierservices.presenter.util;

/**
 * @author Yordan Marinov
 */
public class MultipleFieldValidator {
    private int controlNumber = 0;
    private int numberOfValidations;

    public MultipleFieldValidator(int numberOfValidations) {
        this.numberOfValidations = numberOfValidations;
    }

    public void toggleOn(int bitPos) {
        controlNumber = controlNumber | (1 << bitPos);
    }

    public void toggleOff(int bitPos) {
        int inverseControlNumber = (controlNumber ^ (1 << bitPos));
        controlNumber = controlNumber & inverseControlNumber;
    }

    public boolean isInvalidObject() {
        return controlNumber != (int) (Math.pow(2, numberOfValidations) - 1);
    }

    public void reset() {
        controlNumber = 0;
    }
}
