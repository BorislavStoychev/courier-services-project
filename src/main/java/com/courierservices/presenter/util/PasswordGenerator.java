package com.courierservices.presenter.util;

import java.util.Random;

/**
 * @author Yordan Marinov
 */
public final class PasswordGenerator {
    public static String generatePassword() {
        String template = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder username = new StringBuilder();
        Random generator = new Random(System.currentTimeMillis()*2);
        for (int i = 0; i < 10; i++) {
            username.append(template.charAt(generator.nextInt(template.length())));
        }
        return username.toString();
    }
}
