package com.courierservices.presenter.util;

/**
 * @author Yordan Marinov
 */
public final class PriceCalculator {
    public static double calculatePrice(String packageType, boolean isOfficeAddress) {
        double price = 0.0;
        switch (packageType) {
            case "ENVELOPE":
                price = price + 2.5;
                break;
            case "PARCEL":
                price = price + 5;
                break;
            case "LARGEPACKAGE":
                price = price + 10;
                break;
            case "CARGO":
                price = price + 20;
                break;
        }
        price = price + ((isOfficeAddress) ? 10 : 20);
        return price;
    }
}
