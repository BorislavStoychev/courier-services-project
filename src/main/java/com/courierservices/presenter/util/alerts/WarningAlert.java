package com.courierservices.presenter.util.alerts;

import javafx.scene.control.Alert;

/**
 * @author Yordan Marinov
 */
public class WarningAlert {
    private static Alert alert;

    static {
        alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning");
        alert.setHeaderText(null);
    }

    public static void showAndWait(String contextText) {
        alert.setContentText(contextText);
        alert.showAndWait();
    }

    public static String getContentText() {
        return alert.getContentText();
    }
}
