package com.courierservices.presenter.util;

/**
 * @author Yordan Marinov
 */
public final class StyleConstants {
    public final static String validField = "-fx-background-image: url('css/image/valid-logo.png');" +
            "-fx-background-size: 32px 32px;" +
            "-fx-background-position: right center;" +
            "-fx-background-repeat: no-repeat;" +
            "-fx-border-color: linear-gradient(to right, #ff7f50, #6a5acd);" +
            "-fx-border-width: 0 0 1 0;" +
            "-fx-background-color: transparent";

    public final static String invalidField = "-fx-background-image: url('css/image/invalid-logo.png');" +
            "-fx-background-size: 32px 32px;" +
            "-fx-background-position: right center;" +
            "-fx-background-repeat: no-repeat;" +
            "-fx-border-color: linear-gradient(to right, #ff7f50, #6a5acd);" +
            "-fx-border-width: 0 0 1 0;" +
            "-fx-background-color: transparent";
}
