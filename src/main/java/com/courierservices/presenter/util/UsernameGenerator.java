package com.courierservices.presenter.util;

import java.util.Random;

/**
 * @author Yordan Marinov
 */
public final class UsernameGenerator {
    public static String generateCourierUsername() {
        String template = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder username = new StringBuilder();
        Random generator = new Random(System.currentTimeMillis());
        for (int i = 0; i < 12; i++) {
            username.append(template.charAt(generator.nextInt(template.length())));
        }
        return "cr_" + username;
    }

    public static String generateClientUsername() {
        String template = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder username = new StringBuilder();
        Random generator = new Random(System.currentTimeMillis());
        for (int i = 0; i < 7; i++) {
            username.append(template.charAt(generator.nextInt(template.length())));
        }
        return "cl_" + username;
    }
}
