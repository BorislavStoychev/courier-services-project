package com.courierservices.presenter.util;

import java.sql.Timestamp;

/**
 * @author Yordan Marinov
 */
public final class CurrentTimestamp {
    public static Timestamp get() {
        return new Timestamp(System.currentTimeMillis());
    }
}
