package com.courierservices.presenter.util;

import java.util.Random;

/**
 * @author Yordan Marinov
 */
public final class NumberGenerator {
    private static final Random generator = new Random(System.currentTimeMillis());

    public static int generateSmallNumber() {
        return 1 + generator.nextInt(3);
    }

    public static int generateBigNumber() {
        return 1 + generator.nextInt(5);
    }
}
