package com.courierservices.model.pojo;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

/**
 * @author Yordan Marinov
 */

@Entity
@Table(name = "company")
public final class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "company_gen")
    @SequenceGenerator(
            name = "company_gen",
            sequenceName = "company_seq",
            allocationSize = 1
    )
    @Column(name = "company_id")
    private long companyId;

    @Column(name = "name")
    private String name;

    @Column(name = "inactive_status")
    private boolean inactiveStatus;

    @Column(name = "creation_date")
    private Timestamp creationDate;

    @OneToMany(mappedBy = "company")
    private List<Office> offices;

    @OneToOne(mappedBy = "company", cascade = CascadeType.ALL)
    private Warehouse warehouse;

    @OneToMany(mappedBy = "company")
    private List<Client> clients;

    public Company() {
    }

    public Company(String name, Timestamp creationDate) {
        this.name = name;
        this.creationDate = creationDate;
    }

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isInactiveStatus() {
        return inactiveStatus;
    }

    public void setInactiveStatus(boolean inactiveStatus) {
        this.inactiveStatus = inactiveStatus;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public List<Office> getOffices() {
        return offices;
    }

    public void setOffices(List<Office> offices) {
        this.offices = offices;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    @Override
    public String toString() {
        return this.name + " - " + this.warehouse.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Company company = (Company) o;
        return companyId == company.companyId &&
                inactiveStatus == company.inactiveStatus &&
                name.equals(company.name) &&
                creationDate.equals(company.creationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyId, name, inactiveStatus, creationDate);
    }
}
