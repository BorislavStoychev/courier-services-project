package com.courierservices.model.pojo;

public final class PackageStatus {
    public final static String REGISTERED = "REGISTERED";
    public final static String INTRANSIT = "INTRANSIT";
    public final static String DELIVERED = "DELIVERED";
    public final static String CANCELLED = "CANCELLED";
    public final static String RECEIVED = "RECEIVED";
    //public final static String NOTRECEIVED = "NOTRECEIVED";
}