package com.courierservices.model.pojo;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author Borislav Stoychev
 */

@Entity
@Table(name = "shipment")
public final class Shipment {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "shipment_gen")
    @SequenceGenerator(
            name = "shipment_gen",
            sequenceName = "shipment_seq",
            allocationSize = 1
    )
    @Column(name = "shipment_id")
    private long shipmentId;

    @Column(name = "dispatch_timestamp")
    private Timestamp dispatchTime;

    @OneToMany(mappedBy = "shipment", cascade = CascadeType.ALL)
    private List<Package> packages;

    @ManyToOne
    @JoinColumn(name = "courier_id")
    private Courier courier;

    public Shipment() {
    }

    public long getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(long shipmentId) {
        this.shipmentId = shipmentId;
    }

    public Timestamp getDispatchTime() {
        return dispatchTime;
    }

    public void setDispatchTime(Timestamp dispatchTime) {
        this.dispatchTime = dispatchTime;
    }

    public List<Package> getPackages() {
        return packages;
    }

    public void setPackages(List<Package> packages) {
        this.packages = packages;
    }

    public Courier getCourier() {
        return courier;
    }

    public void setCourier(Courier courier) {
        this.courier = courier;
    }

    @Override
    public String toString() {
        return "Shipment{" +
                "shipmentId=" + shipmentId +
                ", dispatchTime=" + dispatchTime +
                ", packages=" + packages +
                '}';
    }
}