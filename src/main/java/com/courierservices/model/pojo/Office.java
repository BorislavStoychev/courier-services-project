package com.courierservices.model.pojo;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

/**
 * @author Yordan Marinov
 */

@Entity
@Table(name = "office")
public final class Office {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "office_gen")
    @SequenceGenerator(
            name = "office_gen",
            sequenceName = "office_seq",
            allocationSize = 1
    )
    @Column(name = "office_id")
    private long officeId;

    @Column(name = "name")
    private String name;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address address;

    @Column(name = "phone_number")
    private String phoneNumber;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "company_id")
    private Company company;

    @Column(name = "inactive_status")
    private boolean inactiveStatus;

    @OneToMany(mappedBy = "office")
    private List<Courier> couriers;

    @OneToMany(mappedBy = "office")
    private List<Package> packages;

    @Column(name = "creation_date")
    private Timestamp creationDate;

    public Office() {
    }

    public Office(String name, Address address, String phoneNumber, Company company, Timestamp creationDate) {
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.company = company;
        this.creationDate = creationDate;
    }

    public long getOfficeId() {
        return officeId;
    }

    public void setOfficeId(long officeId) {
        this.officeId = officeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public boolean isInactiveStatus() {
        return inactiveStatus;
    }

    public void setInactiveStatus(boolean inactiveStatus) {
        this.inactiveStatus = inactiveStatus;
    }

    public List<Courier> getCouriers() {
        return couriers;
    }

    public void setCouriers(List<Courier> couriers) {
        this.couriers = couriers;
    }

    public List<Package> getPackages() {
        return packages;
    }

    public void setPackages(List<Package> packages) {
        this.packages = packages;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return this.name + " - " + this.address.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Office office = (Office) o;
        return officeId == office.officeId &&
                inactiveStatus == office.inactiveStatus &&
                Objects.equals(name, office.name) &&
                Objects.equals(address, office.address) &&
                Objects.equals(phoneNumber, office.phoneNumber) &&
                Objects.equals(creationDate, office.creationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(officeId, name, address, phoneNumber, inactiveStatus, creationDate);
    }
}
