package com.courierservices.model.pojo;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

/**
 * @author Borislav Stoychev
 */

@Entity
@Table(name = "client")
public final class Client implements User {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_gen")
    @SequenceGenerator(
            name = "client_gen",
            sequenceName = "user_seq",
            allocationSize = 1
    )
    @Column(name = "client_id")
    private long clientId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email_address")
    private String emailAddress;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address address;

    @OneToMany(mappedBy = "sender")
    private List<Package> sentPackages;

    @OneToMany(mappedBy = "receiver")
    private List<Package> receivedPackages;

    @Column(name = "inactive_status")
    private boolean inactiveStatus;

    @Column(name = "creation_date")
    private Timestamp creationDate;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    public Client() {
    }

    public Client(String firstName, String middleName, String lastName, String phoneNumber, String emailAddress, Timestamp creationDate, Address address, Company company) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.creationDate = creationDate;
        this.address = address;
        this.company = company;
    }

    public Company getCompany()
    {
        return company;
    }

    public void setCompany(Company company)
    {
        this.company = company;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public List<Package> getSentPackages() {
        return sentPackages;
    }

    public void setSentPackages(List<Package> sentPackages) {
        this.sentPackages = sentPackages;
    }

    public List<Package> getReceivedPackages() {
        return receivedPackages;
    }

    public void setReceivedPackages(List<Package> receivedPackages) {
        this.receivedPackages = receivedPackages;
    }

    @Override
    public boolean isInactive() {
        return inactiveStatus;
    }

    public void setInactiveStatus(boolean inactiveStatus) {
        this.inactiveStatus = inactiveStatus;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return clientId == client.clientId;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(clientId);
    }

    @Override
    public String toString() {
        return firstName.charAt(0) + ". " + lastName + " - " + emailAddress;
    }
}
