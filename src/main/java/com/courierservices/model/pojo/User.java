package com.courierservices.model.pojo;

/**
 * @author Yordan Marinov
 */
public interface User {
    boolean isInactive();
}
