package com.courierservices.model.pojo;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author Yordan Marinov
 */

@Entity
@Table(name = "warehouse")
public final class Warehouse {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "warehouse_gen")
    @SequenceGenerator(
            name = "warehouse_gen",
            sequenceName = "warehouse_seq",
            allocationSize = 1
    )
    @Column(name = "warehouse_id")
    @JoinColumn(name = "warehouse_id")
    private long warehouseId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address address;

    @OneToOne
    @JoinColumn(name = "company_id")
    private Company company;

    public Warehouse() {
    }

    public Warehouse(Address address, Company company) {
        this.address = address;
        this.company = company;
    }

    public long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return this.address.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Warehouse warehouse = (Warehouse) o;
        return warehouseId == warehouse.warehouseId &&
                address.equals(warehouse.address) &&
                company.equals(warehouse.company);
    }

    @Override
    public int hashCode() {
        return Objects.hash(warehouseId, address, company);
    }
}