package com.courierservices.model.pojo;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

/**
 * @author Yordan Marinov
 */

@Entity
@Table(name = "courier")
public final class Courier implements User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_gen")
    @SequenceGenerator(
            name = "client_gen",
            sequenceName = "user_seq",
            allocationSize = 1
    )
    @Column(name = "courier_id")
    private long courierId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email_address")
    private String emailAddress;

    @OneToMany(mappedBy = "courier")
    private List<Shipment> shipments;

    @ManyToOne
    @JoinColumn(name = "office_id")
    private Office office;

    @Column(name = "inactive_status")
    private boolean inactiveStatus;

    @Column(name = "creation_date")
    private Timestamp creationDate;

    public Courier() {
    }

    public Courier(String firstName, String middleName, String lastName, String phoneNumber, String emailAddress, Office office, Timestamp creationDate) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.office = office;
        this.creationDate = creationDate;
    }

    public long getCourierId() {
        return courierId;
    }

    public void setCourierId(long courierId) {
        this.courierId = courierId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public List<Shipment> getShipments() {
        return shipments;
    }

    public void setShipments(List<Shipment> shipments) {
        this.shipments = shipments;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    @Override
    public boolean isInactive() {
        return inactiveStatus;
    }

    public void setInactiveStatus(boolean inactiveStatus) {
        this.inactiveStatus = inactiveStatus;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return this.firstName + " " + this.lastName + " - " + this.office.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Courier courier = (Courier) o;
        return courierId == courier.courierId &&
                inactiveStatus == courier.inactiveStatus &&
                Objects.equals(firstName, courier.firstName) &&
                Objects.equals(middleName, courier.middleName) &&
                Objects.equals(lastName, courier.lastName) &&
                Objects.equals(phoneNumber, courier.phoneNumber) &&
                Objects.equals(emailAddress, courier.emailAddress) &&
                Objects.equals(creationDate, courier.creationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(courierId, firstName, middleName, lastName, phoneNumber, emailAddress, office, inactiveStatus, creationDate);
    }
}
