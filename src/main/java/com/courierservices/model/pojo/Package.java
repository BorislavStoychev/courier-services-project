package com.courierservices.model.pojo;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * @author Borislav Stoychev
 */

@Entity
@Table(name = "package")
public final class Package {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "package_gen")
    @SequenceGenerator(
            name = "package_gen",
            sequenceName = "package_seq",
            allocationSize = 1
    )
    @Column(name = "package_id")
    private long packageId;

    @Column(name = "type")
    private String type;

    @Column(name = "price")
    private double price;

    @Column(name = "status")
    private String status;

    @OneToOne
    @JoinColumn(name = "receiver_address_id")
    private Address receiverAddress;

    @ManyToOne
    @JoinColumn(name = "sender_id")
    private Client sender;

    @ManyToOne
    @JoinColumn(name = "receiver_id")
    private Client receiver;

    @ManyToOne
    @JoinColumn(name = "shipment_id")
    private Shipment shipment;

    @ManyToOne
    @JoinColumn(name = "office_id")
    private Office office;

    @Column(name = "arrival_timestamp")
    private Timestamp arrivalTime;

    @Column(name = "creation_date")
    private Timestamp creationDate;

    public Package() {
    }

    public Package(String type, double price, String status, Address receiverAddress, Client sender, Client receiver, Office office, Timestamp creationDate) {
        this.type = type;
        this.price = price;
        this.status = status;
        this.receiverAddress = receiverAddress;
        this.sender = sender;
        this.receiver = receiver;
        this.office = office;
        this.creationDate = creationDate;
    }

    public long getPackageId() {
        return packageId;
    }

    public void setPackageId(long packageId) {
        this.packageId = packageId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Address getReceiverAddress() {
        return receiverAddress;
    }

    public void setReceiverAddress(Address receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    public Client getSender() {
        return sender;
    }

    public void setSender(Client sender) {
        this.sender = sender;
    }

    public Client getReceiver() {
        return receiver;
    }

    public void setReceiver(Client receiver) {
        this.receiver = receiver;
    }

    public Shipment getShipment() {
        return shipment;
    }

    public void setShipment(Shipment shipment) {
        this.shipment = shipment;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    public Timestamp getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Timestamp arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return "Type: " + type +
                " From: " + sender +
                " Receiver: " + receiver +
                " Destination address: " + receiverAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Package pack = (Package) o;
        return packageId == pack.packageId &&
                Double.compare(pack.price, price) == 0 &&
                Objects.equals(type, pack.type) &&
                Objects.equals(status, pack.status) &&
                Objects.equals(receiverAddress, pack.receiverAddress) &&
                Objects.equals(sender, pack.sender) &&
                Objects.equals(receiver, pack.receiver) &&
                Objects.equals(creationDate, pack.creationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(packageId, type, price, status, receiverAddress, sender, receiver, creationDate);
    }
}
