package com.courierservices.model.pojo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class PackageType {
    private final static String ENVELOPE = "ENVELOPE";
    private final static String PARCEL = "PARCEL";
    private final static String LARGEPACKAGE = "LARGEPACKAGE";
    private final static String CARGO = "CARGO";

    public static List<String> getAll() {
        return new ArrayList<>(Arrays.asList(ENVELOPE, PARCEL, LARGEPACKAGE, CARGO));
    }
}