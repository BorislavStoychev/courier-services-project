package com.courierservices.model.connection;

import com.courierservices.model.pojo.Package;
import com.courierservices.model.pojo.*;
import org.hibernate.cfg.Configuration;

/**
 * @author Yordan Marinov
 */

public final class HibernateConfiguration {

    private static Configuration cfg;

    public static void init() {
        cfg = new Configuration()
                .addAnnotatedClass(Company.class)
                .addAnnotatedClass(Office.class)
                .addAnnotatedClass(Address.class)
                .addAnnotatedClass(Warehouse.class)
                .addAnnotatedClass(Courier.class)
                .addAnnotatedClass(Credentials.class)
                .addAnnotatedClass(Package.class)
                .addAnnotatedClass(Shipment.class)
                .addAnnotatedClass(PackageStatus.class)
                .addAnnotatedClass(PackageType.class)
                .addAnnotatedClass(Client.class);
    }

    static Configuration getConfiguration() {
        return cfg;
    }
}
