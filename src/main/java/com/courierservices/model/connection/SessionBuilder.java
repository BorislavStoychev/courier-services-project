package com.courierservices.model.connection;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * @author Yordan Marinov
 */
public final class SessionBuilder {
    private static SessionFactory sessionFactory;

    public static void init() throws HibernateException {
        sessionFactory = HibernateConfiguration.getConfiguration().buildSessionFactory();
    }

    public static Session getSession() {
        return sessionFactory.openSession();
    }

    public static void close() {
        sessionFactory.close();
    }
}
