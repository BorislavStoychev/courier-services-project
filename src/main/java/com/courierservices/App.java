package com.courierservices;

import com.courierservices.view.ViewManager;
import javafx.application.Application;
import javafx.stage.Stage;


/**
 * @author Yordan Marinov
 */
public final class App extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        new ViewManager(primaryStage);
    }
}
