package com.courierservices.view;

import com.courierservices.model.connection.HibernateConfiguration;
import com.courierservices.model.connection.SessionBuilder;
import com.courierservices.presenter.controller.subcontroller.ClientCreateController;
import com.courierservices.presenter.util.alerts.ErrorAlert;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;

/**
 * @author Yordan Marinov
 */
public final class ViewManager {
    public static Scene activeScene;
    private static Stage applicationStage;
    private static Scene loginScene;
    private static final Logger logger
            = LoggerFactory.getLogger(ClientCreateController.class);

    public ViewManager(Stage primaryStage) {
        applicationStage = primaryStage;
        try {
            URL url = getClass().getClassLoader().getResource("fxml/login.fxml");
            if (url != null) {
                loginScene = new Scene(FXMLLoader.load((url))); // 640 480
                loadScene(loginScene);
                HibernateConfiguration.init();
                SessionBuilder.init();
                initOnClose();
                showScene();
            } else {
                throw new NullPointerException("Couldn't find and load view.");
            }
        } catch (NullPointerException | IOException | HibernateException exception) {
            ErrorAlert.showAndWait("Error while trying to connect.");
            logger.error(exception.getMessage());
        }
    }

    private void initOnClose() {
        applicationStage.setOnCloseRequest(event -> SessionBuilder.close());
    }

    private void showScene() {
        applicationStage.setTitle("Courier services");
        applicationStage.setResizable(false);
        applicationStage.getIcons().add(new Image("/css/image/login-logo.png"));
        applicationStage.show();
    }

    public static void loadScene(Scene scene) {
        applicationStage.setScene(scene);
    }


    public static void resetScene() {
        applicationStage.setScene(loginScene);
        activeScene = null;
    }
}
