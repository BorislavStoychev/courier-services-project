import com.courierservices.presenter.util.PriceCalculator;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Yordan Marinov
 */
public class PriceCalculatorTests {

    @Test
    public void testSuccessfulPriceCalculation() {
        double expected = PriceCalculator.calculatePrice("CARGO", false);
        Assert.assertEquals(expected, 40.0, 0.0);
    }

    @Test
    public void testFailedPriceCalculation() {
        double expected = PriceCalculator.calculatePrice("CARGO", false);
        Assert.assertNotEquals(expected, 20.0, 0.0);
    }
}
