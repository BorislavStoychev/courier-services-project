import com.courierservices.presenter.util.MultipleFieldValidator;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Yordan Marinov
 */
public class MultipleFieldValidatorTests {
    @Test
    public void testSuccessfulMultipleFieldValidation() {
        MultipleFieldValidator mfv = new MultipleFieldValidator(3);
        mfv.toggleOn(0);
        mfv.toggleOn(1);
        mfv.toggleOn(2);
        Assert.assertFalse(mfv.isInvalidObject());
    }

    @Test
    public void testFailedMultipleFieldValidation() {
        MultipleFieldValidator mfv = new MultipleFieldValidator(3);
        mfv.toggleOn(0);
        mfv.toggleOff(1);
        mfv.toggleOn(2);
        Assert.assertTrue(mfv.isInvalidObject());
    }
}
