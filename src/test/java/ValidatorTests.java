import com.courierservices.presenter.util.FieldValidator;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Yordan Marinov
 */
public class ValidatorTests {

    @Test
    public void testSuccessfulTextValidation() {
        String value = "Some value";
        boolean result = FieldValidator.isValidText(value);
        Assert.assertTrue(result);
    }

    @Test
    public void testFailedTextValidation () {
        String value = "Some value 1";
        boolean result = FieldValidator.isValidText(value);
        Assert.assertFalse(result);
    }

    @Test
    public void testEmptyStringTextValidation() {
        String value = "";
        boolean result = FieldValidator.isValidText(value);
        Assert.assertFalse(result);
    }

    @Test
    public void testSuccessfulPostalCodeValidation() {
        String value = "7923";
        boolean result = FieldValidator.isValidPostCode(value);
        Assert.assertTrue(result);
    }

    @Test
    public void testFailedPostalCodeValidation () {
        String value = "532s";
        boolean result = FieldValidator.isValidText(value);
        Assert.assertFalse(result);
    }

    @Test
    public void testEmptyStringPostalCodeValidation() {
        String value = "";
        boolean result = FieldValidator.isValidPostCode(value);
        Assert.assertFalse(result);
    }

    @Test
    public void testSuccessfulPhoneNumberValidation() {
        String value = "0889095061";
        boolean result = FieldValidator.isValidPhoneNumber(value);
        Assert.assertTrue(result);
    }

    @Test
    public void testFailedPhoneNumberValidation () {
        String value = "0887asb36s3";
        boolean result = FieldValidator.isValidPhoneNumber(value);
        Assert.assertFalse(result);
    }

    @Test
    public void testEmptyStringPhoneNumberValidation() {
        String value = "";
        boolean result = FieldValidator.isValidPhoneNumber(value);
        Assert.assertFalse(result);
    }

    @Test
    public void testSuccessfulEmailAddressValidation() {
        String value = "ymarinov@abv.bg";
        boolean result = FieldValidator.isValidEmailAddress(value);
        Assert.assertTrue(result);
    }

    @Test
    public void testFailedEmailAddressValidation () {
        String value = "ymarinov";
        boolean result = FieldValidator.isValidEmailAddress(value);
        Assert.assertFalse(result);
    }

    @Test
    public void testEmptyStringEmailAddressValidation() {
        String value = "";
        boolean result = FieldValidator.isValidEmailAddress(value);
        Assert.assertFalse(result);
    }

    @Test
    public void testSuccessfulStreetValidation() {
        String value = "Dubrovnik 18";
        boolean result = FieldValidator.isValidStreet(value);
        Assert.assertTrue(result);
    }

    @Test
    public void testFailedStreetValidation () {
        String value = "Dubrovnik";
        boolean result = FieldValidator.isValidStreet(value);
        Assert.assertFalse(result);
    }

    @Test
    public void testEmptyStringStreetValidation() {
        String value = "";
        boolean result = FieldValidator.isValidStreet(value);
        Assert.assertFalse(result);
    }
}
